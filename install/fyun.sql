/*
Navicat MySQL Data Transfer

Source Server         : http://115.28.133.173/_3306
Source Server Version : 50537
Source Host           : 115.28.133.173:3306
Source Database       : weixin

Target Server Type    : MYSQL
Target Server Version : 50537
File Encoding         : 65001

Date: 2014-08-08 10:07:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for si_address
-- ----------------------------
DROP TABLE IF EXISTS `si_address`;
CREATE TABLE `si_address` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `lastname` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `company` varchar(32) COLLATE utf8_bin NOT NULL,
  `mobile` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `address_1` varchar(128) COLLATE utf8_bin NOT NULL,
  `address_2` varchar(128) COLLATE utf8_bin NOT NULL,
  `city` varchar(128) COLLATE utf8_bin NOT NULL,
  `city_id` int(11) NOT NULL DEFAULT '0',
  `postcode` varchar(10) COLLATE utf8_bin NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `zone_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`address_id`),
  KEY `customer_id` (`customer_id`)
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_address
-- ----------------------------

-- ----------------------------
-- Table structure for si_affiliate
-- ----------------------------
DROP TABLE IF EXISTS `si_affiliate`;
CREATE TABLE `si_affiliate` (
  `affiliate_id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `lastname` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `email` varchar(96) COLLATE utf8_bin NOT NULL DEFAULT '',
  `telephone` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `fax` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `password` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '',
  `company` varchar(32) COLLATE utf8_bin NOT NULL,
  `website` varchar(255) COLLATE utf8_bin NOT NULL,
  `address_1` varchar(128) COLLATE utf8_bin NOT NULL DEFAULT '',
  `address_2` varchar(128) COLLATE utf8_bin NOT NULL,
  `city` varchar(128) COLLATE utf8_bin NOT NULL DEFAULT '',
  `postcode` varchar(10) COLLATE utf8_bin NOT NULL DEFAULT '',
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  `code` varchar(64) COLLATE utf8_bin NOT NULL,
  `commission` decimal(4,2) NOT NULL DEFAULT '0.00',
  `tax` varchar(64) COLLATE utf8_bin NOT NULL,
  `payment` varchar(6) COLLATE utf8_bin NOT NULL,
  `cheque` varchar(100) COLLATE utf8_bin NOT NULL DEFAULT '',
  `paypal` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `bank_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `bank_branch_number` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `bank_swift_code` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `bank_account_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `bank_account_number` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `ip` varchar(15) COLLATE utf8_bin NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`affiliate_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_affiliate
-- ----------------------------

-- ----------------------------
-- Table structure for si_affiliate_transaction
-- ----------------------------
DROP TABLE IF EXISTS `si_affiliate_transaction`;
CREATE TABLE `si_affiliate_transaction` (
  `affiliate_transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `affiliate_id` int(11) NOT NULL,
  `order_id` varchar(128) COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`affiliate_transaction_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_affiliate_transaction
-- ----------------------------

-- ----------------------------
-- Table structure for si_article
-- ----------------------------
DROP TABLE IF EXISTS `si_article`;
CREATE TABLE `si_article` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_bin NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_bin NOT NULL,
  `content` text COLLATE utf8_bin NOT NULL,
  `status` int(11) DEFAULT '0',
  `download_only` int(1) NOT NULL DEFAULT '0',
  `sort_order` int(11) DEFAULT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`article_id`,`language_id`),
  KEY `name` (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_article
-- ----------------------------

-- ----------------------------
-- Table structure for si_article_category
-- ----------------------------
DROP TABLE IF EXISTS `si_article_category`;
CREATE TABLE `si_article_category` (
  `article_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `download_only` int(1) NOT NULL DEFAULT '0',
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` int(1) NOT NULL DEFAULT '1',
  `type` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`article_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_article_category
-- ----------------------------

-- ----------------------------
-- Table structure for si_article_category_description
-- ----------------------------
DROP TABLE IF EXISTS `si_article_category_description`;
CREATE TABLE `si_article_category_description` (
  `article_category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `meta_keywords` varchar(255) COLLATE utf8_bin NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`article_category_id`,`language_id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_article_category_description
-- ----------------------------

-- ----------------------------
-- Table structure for si_article_category_to_layout
-- ----------------------------
DROP TABLE IF EXISTS `si_article_category_to_layout`;
CREATE TABLE `si_article_category_to_layout` (
  `article_category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  PRIMARY KEY (`article_category_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_article_category_to_layout
-- ----------------------------

-- ----------------------------
-- Table structure for si_article_category_to_store
-- ----------------------------
DROP TABLE IF EXISTS `si_article_category_to_store`;
CREATE TABLE `si_article_category_to_store` (
  `article_category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`article_category_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_article_category_to_store
-- ----------------------------

-- ----------------------------
-- Table structure for si_article_related
-- ----------------------------
DROP TABLE IF EXISTS `si_article_related`;
CREATE TABLE `si_article_related` (
  `article_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL,
  PRIMARY KEY (`article_id`,`related_id`),
  KEY `news_id` (`article_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_article_related
-- ----------------------------

-- ----------------------------
-- Table structure for si_article_tags
-- ----------------------------
DROP TABLE IF EXISTS `si_article_tags`;
CREATE TABLE `si_article_tags` (
  `article_id` int(11) NOT NULL,
  `tag` varchar(32) COLLATE utf8_bin NOT NULL,
  `language_id` int(11) NOT NULL,
  PRIMARY KEY (`article_id`,`tag`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_article_tags
-- ----------------------------

-- ----------------------------
-- Table structure for si_article_to_category
-- ----------------------------
DROP TABLE IF EXISTS `si_article_to_category`;
CREATE TABLE `si_article_to_category` (
  `article_id` int(11) NOT NULL,
  `article_category_id` int(11) NOT NULL,
  PRIMARY KEY (`article_id`,`article_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_article_to_category
-- ----------------------------

-- ----------------------------
-- Table structure for si_article_to_download
-- ----------------------------
DROP TABLE IF EXISTS `si_article_to_download`;
CREATE TABLE `si_article_to_download` (
  `article_id` int(11) NOT NULL,
  `download_id` int(11) NOT NULL,
  PRIMARY KEY (`article_id`,`download_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_article_to_download
-- ----------------------------

-- ----------------------------
-- Table structure for si_article_to_layout
-- ----------------------------
DROP TABLE IF EXISTS `si_article_to_layout`;
CREATE TABLE `si_article_to_layout` (
  `article_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  PRIMARY KEY (`article_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_article_to_layout
-- ----------------------------

-- ----------------------------
-- Table structure for si_attribute
-- ----------------------------
DROP TABLE IF EXISTS `si_attribute`;
CREATE TABLE `si_attribute` (
  `attribute_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`attribute_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_attribute
-- ----------------------------

-- ----------------------------
-- Table structure for si_attribute_description
-- ----------------------------
DROP TABLE IF EXISTS `si_attribute_description`;
CREATE TABLE `si_attribute_description` (
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`attribute_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_attribute_description
-- ----------------------------

-- ----------------------------
-- Table structure for si_attribute_group
-- ----------------------------
DROP TABLE IF EXISTS `si_attribute_group`;
CREATE TABLE `si_attribute_group` (
  `attribute_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`attribute_group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_attribute_group
-- ----------------------------

-- ----------------------------
-- Table structure for si_attribute_group_description
-- ----------------------------
DROP TABLE IF EXISTS `si_attribute_group_description`;
CREATE TABLE `si_attribute_group_description` (
  `attribute_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`attribute_group_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_attribute_group_description
-- ----------------------------

-- ----------------------------
-- Table structure for si_banner
-- ----------------------------
DROP TABLE IF EXISTS `si_banner`;
CREATE TABLE `si_banner` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_bin NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`banner_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_banner
-- ----------------------------
INSERT INTO `si_banner` VALUES ('9', '首页横幅', '1');

-- ----------------------------
-- Table structure for si_banner_image
-- ----------------------------
DROP TABLE IF EXISTS `si_banner_image`;
CREATE TABLE `si_banner_image` (
  `banner_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_id` int(11) NOT NULL,
  `link` varchar(255) COLLATE utf8_bin NOT NULL,
  `image` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`banner_image_id`)
) ENGINE=MyISAM AUTO_INCREMENT=113 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_banner_image
-- ----------------------------

-- ----------------------------
-- Table structure for si_banner_image_description
-- ----------------------------
DROP TABLE IF EXISTS `si_banner_image_description`;
CREATE TABLE `si_banner_image_description` (
  `banner_image_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `title` varchar(64) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`banner_image_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_banner_image_description
-- ----------------------------

-- ----------------------------
-- Table structure for si_category
-- ----------------------------
DROP TABLE IF EXISTS `si_category`;
CREATE TABLE `si_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `top` tinyint(1) NOT NULL DEFAULT '0',
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=87 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_category
-- ----------------------------

-- ----------------------------
-- Table structure for si_category_description
-- ----------------------------
DROP TABLE IF EXISTS `si_category_description`;
CREATE TABLE `si_category_description` (
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `description` text COLLATE utf8_bin NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_bin NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`category_id`,`language_id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_category_description
-- ----------------------------

-- ----------------------------
-- Table structure for si_category_to_layout
-- ----------------------------
DROP TABLE IF EXISTS `si_category_to_layout`;
CREATE TABLE `si_category_to_layout` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_category_to_layout
-- ----------------------------

-- ----------------------------
-- Table structure for si_category_to_store
-- ----------------------------
DROP TABLE IF EXISTS `si_category_to_store`;
CREATE TABLE `si_category_to_store` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_category_to_store
-- ----------------------------

-- ----------------------------
-- Table structure for si_city
-- ----------------------------
DROP TABLE IF EXISTS `si_city`;
CREATE TABLE `si_city` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `zone_id` int(11) NOT NULL,
  `code` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `name` varchar(128) COLLATE utf8_bin NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `center_status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`city_id`)
) ENGINE=MyISAM AUTO_INCREMENT=431 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_city
-- ----------------------------
INSERT INTO `si_city` VALUES ('427', '44', '718', '', '西大直街', '1', '0');
INSERT INTO `si_city` VALUES ('428', '44', '718', '', '服装城', '1', '0');
INSERT INTO `si_city` VALUES ('429', '44', '719', '', '红旗大街', '1', '0');
INSERT INTO `si_city` VALUES ('430', '44', '719', '', '家具城', '1', '0');

-- ----------------------------
-- Table structure for si_country
-- ----------------------------
DROP TABLE IF EXISTS `si_country`;
CREATE TABLE `si_country` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_bin NOT NULL,
  `iso_code_2` varchar(2) COLLATE utf8_bin NOT NULL DEFAULT '',
  `iso_code_3` varchar(3) COLLATE utf8_bin NOT NULL DEFAULT '',
  `address_format` text COLLATE utf8_bin NOT NULL,
  `postcode_required` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`country_id`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_country
-- ----------------------------
INSERT INTO `si_country` VALUES ('44', '哈尔滨', '', '', '', '0', '1');

-- ----------------------------
-- Table structure for si_coupon
-- ----------------------------
DROP TABLE IF EXISTS `si_coupon`;
CREATE TABLE `si_coupon` (
  `coupon_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_bin NOT NULL,
  `code` varchar(10) COLLATE utf8_bin NOT NULL,
  `type` char(1) COLLATE utf8_bin NOT NULL,
  `discount` decimal(15,4) NOT NULL,
  `logged` tinyint(1) NOT NULL DEFAULT '0',
  `shipping` tinyint(1) NOT NULL DEFAULT '0',
  `total` decimal(15,4) NOT NULL,
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  `uses_total` int(11) NOT NULL,
  `uses_customer` varchar(11) COLLATE utf8_bin NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`coupon_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_coupon
-- ----------------------------

-- ----------------------------
-- Table structure for si_coupon_history
-- ----------------------------
DROP TABLE IF EXISTS `si_coupon_history`;
CREATE TABLE `si_coupon_history` (
  `coupon_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` int(11) NOT NULL,
  `order_id` varchar(128) COLLATE utf8_bin NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`coupon_history_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_coupon_history
-- ----------------------------

-- ----------------------------
-- Table structure for si_coupon_product
-- ----------------------------
DROP TABLE IF EXISTS `si_coupon_product`;
CREATE TABLE `si_coupon_product` (
  `coupon_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`coupon_product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_coupon_product
-- ----------------------------

-- ----------------------------
-- Table structure for si_currency
-- ----------------------------
DROP TABLE IF EXISTS `si_currency`;
CREATE TABLE `si_currency` (
  `currency_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `code` varchar(3) COLLATE utf8_bin NOT NULL DEFAULT '',
  `symbol_left` varchar(12) COLLATE utf8_bin NOT NULL,
  `symbol_right` varchar(12) COLLATE utf8_bin NOT NULL,
  `decimal_place` char(1) COLLATE utf8_bin NOT NULL,
  `value` float(15,8) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`currency_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_currency
-- ----------------------------
INSERT INTO `si_currency` VALUES ('1', '人民币', 'CNY', '', '', '2', '1.00000000', '1', '2014-08-08 10:06:45');

-- ----------------------------
-- Table structure for si_customer
-- ----------------------------
DROP TABLE IF EXISTS `si_customer`;
CREATE TABLE `si_customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `open_id` varchar(96) COLLATE utf8_bin NOT NULL,
  `username` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `firstname` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `lastname` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `email` varchar(96) COLLATE utf8_bin NOT NULL DEFAULT '',
  `phone_bind` int(11) NOT NULL DEFAULT '0',
  `telephone` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `fax` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `password` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '',
  `cart` text COLLATE utf8_bin,
  `wishlist` text COLLATE utf8_bin,
  `newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `address` varchar(160) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL,
  `ip` varchar(15) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `active_code` varchar(160) COLLATE utf8_bin DEFAULT NULL,
  `code` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `shipping_method` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `payment_method` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `address_id` int(10) DEFAULT NULL,
  `zone_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_customer
-- ----------------------------

-- ----------------------------
-- Table structure for si_customer_group
-- ----------------------------
DROP TABLE IF EXISTS `si_customer_group`;
CREATE TABLE `si_customer_group` (
  `customer_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`customer_group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_customer_group
-- ----------------------------
INSERT INTO `si_customer_group` VALUES ('8', '银卡会员');
INSERT INTO `si_customer_group` VALUES ('6', '金卡会员');

-- ----------------------------
-- Table structure for si_customer_ip
-- ----------------------------
DROP TABLE IF EXISTS `si_customer_ip`;
CREATE TABLE `si_customer_ip` (
  `customer_ip_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `ip` varchar(15) COLLATE utf8_bin NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_ip_id`),
  KEY `ip` (`ip`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_customer_ip
-- ----------------------------

-- ----------------------------
-- Table structure for si_customer_reward
-- ----------------------------
DROP TABLE IF EXISTS `si_customer_reward`;
CREATE TABLE `si_customer_reward` (
  `customer_reward_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `order_id` varchar(128) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_bin NOT NULL,
  `points` int(8) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`customer_reward_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_customer_reward
-- ----------------------------

-- ----------------------------
-- Table structure for si_customer_transaction
-- ----------------------------
DROP TABLE IF EXISTS `si_customer_transaction`;
CREATE TABLE `si_customer_transaction` (
  `customer_transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `order_id` varchar(128) COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_transaction_id`)
) ENGINE=MyISAM AUTO_INCREMENT=123 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_customer_transaction
-- ----------------------------

-- ----------------------------
-- Table structure for si_download
-- ----------------------------
DROP TABLE IF EXISTS `si_download`;
CREATE TABLE `si_download` (
  `download_id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(128) COLLATE utf8_bin NOT NULL DEFAULT '',
  `mask` varchar(128) COLLATE utf8_bin NOT NULL DEFAULT '',
  `remaining` int(11) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`download_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_download
-- ----------------------------

-- ----------------------------
-- Table structure for si_download_description
-- ----------------------------
DROP TABLE IF EXISTS `si_download_description`;
CREATE TABLE `si_download_description` (
  `download_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`download_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_download_description
-- ----------------------------

-- ----------------------------
-- Table structure for si_extension
-- ----------------------------
DROP TABLE IF EXISTS `si_extension`;
CREATE TABLE `si_extension` (
  `extension_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) COLLATE utf8_bin NOT NULL,
  `code` varchar(32) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`extension_id`)
) ENGINE=MyISAM AUTO_INCREMENT=479 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_extension
-- ----------------------------
INSERT INTO `si_extension` VALUES ('23', 'payment', 'cod');
INSERT INTO `si_extension` VALUES ('22', 'total', 'shipping');
INSERT INTO `si_extension` VALUES ('57', 'total', 'sub_total');
INSERT INTO `si_extension` VALUES ('58', 'total', 'tax');
INSERT INTO `si_extension` VALUES ('59', 'total', 'total');
INSERT INTO `si_extension` VALUES ('410', 'module', 'banner');
INSERT INTO `si_extension` VALUES ('426', 'module', 'carousel');
INSERT INTO `si_extension` VALUES ('390', 'total', 'credit');
INSERT INTO `si_extension` VALUES ('349', 'total', 'handling');
INSERT INTO `si_extension` VALUES ('478', 'shipping', 'weight');
INSERT INTO `si_extension` VALUES ('389', 'total', 'coupon');
INSERT INTO `si_extension` VALUES ('413', 'module', 'category');
INSERT INTO `si_extension` VALUES ('408', 'module', 'account');
INSERT INTO `si_extension` VALUES ('393', 'total', 'reward');
INSERT INTO `si_extension` VALUES ('453', 'module', 'affiliate');
INSERT INTO `si_extension` VALUES ('419', 'module', 'slideshow');
INSERT INTO `si_extension` VALUES ('429', 'module', 'cates');
INSERT INTO `si_extension` VALUES ('438', 'module', 'latest');
INSERT INTO `si_extension` VALUES ('466', 'module', 'information');
INSERT INTO `si_extension` VALUES ('465', 'module', 'hotsell');
INSERT INTO `si_extension` VALUES ('449', 'module', 'bestseller');
INSERT INTO `si_extension` VALUES ('454', 'module', 'mostviewed');
INSERT INTO `si_extension` VALUES ('464', 'module', 'dealday');
INSERT INTO `si_extension` VALUES ('459', 'module', 'viewed');
INSERT INTO `si_extension` VALUES ('469', 'feed', 'google_base');
INSERT INTO `si_extension` VALUES ('471', 'module', 'onlineim');
INSERT INTO `si_extension` VALUES ('467', 'module', 'store');
INSERT INTO `si_extension` VALUES ('468', 'module', 'welcome');
INSERT INTO `si_extension` VALUES ('470', 'feed', 'google_sitemap');
INSERT INTO `si_extension` VALUES ('472', 'total', 'low_order_fee');
INSERT INTO `si_extension` VALUES ('473', 'total', 'voucher');

-- ----------------------------
-- Table structure for si_geo_zone
-- ----------------------------
DROP TABLE IF EXISTS `si_geo_zone`;
CREATE TABLE `si_geo_zone` (
  `geo_zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`geo_zone_id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_geo_zone
-- ----------------------------
INSERT INTO `si_geo_zone` VALUES ('17', '5元送餐区', '5元送餐区', '2014-04-06 03:03:41', '2014-02-20 14:44:37');
INSERT INTO `si_geo_zone` VALUES ('18', '10元送餐区', '10元送餐区', '0000-00-00 00:00:00', '2014-04-06 03:04:16');

-- ----------------------------
-- Table structure for si_information
-- ----------------------------
DROP TABLE IF EXISTS `si_information`;
CREATE TABLE `si_information` (
  `information_id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`information_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_information
-- ----------------------------
INSERT INTO `si_information` VALUES ('3', '3', '1');
INSERT INTO `si_information` VALUES ('4', '1', '1');
INSERT INTO `si_information` VALUES ('5', '4', '1');
INSERT INTO `si_information` VALUES ('6', '2', '1');

-- ----------------------------
-- Table structure for si_information_description
-- ----------------------------
DROP TABLE IF EXISTS `si_information_description`;
CREATE TABLE `si_information_description` (
  `information_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `description` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`information_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_information_description
-- ----------------------------
INSERT INTO `si_information_description` VALUES ('4', '1', '关于我们', 0x266C743B702667743B0D0A09E585B3E4BA8EE68891E4BBAC266C743B2F702667743B0D0A);
INSERT INTO `si_information_description` VALUES ('5', '1', '相关条款', 0x266C743B702667743B0D0A09E79BB8E585B3E69DA1E6ACBE266C743B2F702667743B0D0A);
INSERT INTO `si_information_description` VALUES ('3', '1', '购买说明', 0x266C743B702667743B0D0A09E8B4ADE4B9B0E69DA1E6ACBE266C743B2F702667743B0D0A);
INSERT INTO `si_information_description` VALUES ('6', '1', '送货说明', 0x266C743B702667743B0D0A09E98081E8B4A7E8AFB4E6988E266C743B2F702667743B0D0A);

-- ----------------------------
-- Table structure for si_information_to_layout
-- ----------------------------
DROP TABLE IF EXISTS `si_information_to_layout`;
CREATE TABLE `si_information_to_layout` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  PRIMARY KEY (`information_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_information_to_layout
-- ----------------------------

-- ----------------------------
-- Table structure for si_information_to_store
-- ----------------------------
DROP TABLE IF EXISTS `si_information_to_store`;
CREATE TABLE `si_information_to_store` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`information_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_information_to_store
-- ----------------------------
INSERT INTO `si_information_to_store` VALUES ('3', '0');
INSERT INTO `si_information_to_store` VALUES ('4', '0');
INSERT INTO `si_information_to_store` VALUES ('5', '0');
INSERT INTO `si_information_to_store` VALUES ('6', '0');

-- ----------------------------
-- Table structure for si_invited_history
-- ----------------------------
DROP TABLE IF EXISTS `si_invited_history`;
CREATE TABLE `si_invited_history` (
  `invited_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `invited_id` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`invited_history_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of si_invited_history
-- ----------------------------

-- ----------------------------
-- Table structure for si_ireturn
-- ----------------------------
DROP TABLE IF EXISTS `si_ireturn`;
CREATE TABLE `si_ireturn` (
  `return_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(128) COLLATE utf8_bin NOT NULL,
  `date_ordered` date NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) COLLATE utf8_bin NOT NULL,
  `lastname` varchar(32) COLLATE utf8_bin NOT NULL,
  `email` varchar(96) COLLATE utf8_bin NOT NULL,
  `telephone` varchar(32) COLLATE utf8_bin NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `comment` text COLLATE utf8_bin,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`return_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_ireturn
-- ----------------------------

-- ----------------------------
-- Table structure for si_language
-- ----------------------------
DROP TABLE IF EXISTS `si_language`;
CREATE TABLE `si_language` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `code` varchar(5) COLLATE utf8_bin NOT NULL,
  `locale` varchar(255) COLLATE utf8_bin NOT NULL,
  `image` varchar(64) COLLATE utf8_bin NOT NULL,
  `directory` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `filename` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`language_id`),
  KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_language
-- ----------------------------
INSERT INTO `si_language` VALUES ('1', '简体中文', 'cn', 'zh,zh-hk,zh-cn,zh-cn.UTF-8,cn-gb,chinese', 'cn.png', 'zh-cn', 'chinese', '1', '1');

-- ----------------------------
-- Table structure for si_layout
-- ----------------------------
DROP TABLE IF EXISTS `si_layout`;
CREATE TABLE `si_layout` (
  `layout_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`layout_id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_layout
-- ----------------------------
INSERT INTO `si_layout` VALUES ('1', 'Home');
INSERT INTO `si_layout` VALUES ('2', 'Product');
INSERT INTO `si_layout` VALUES ('3', 'Category');
INSERT INTO `si_layout` VALUES ('4', 'Default');
INSERT INTO `si_layout` VALUES ('5', 'Manufacturer');
INSERT INTO `si_layout` VALUES ('6', 'Account');
INSERT INTO `si_layout` VALUES ('7', 'Checkout');
INSERT INTO `si_layout` VALUES ('8', 'Contact');
INSERT INTO `si_layout` VALUES ('9', 'Sitemap');
INSERT INTO `si_layout` VALUES ('10', 'Affiliate');
INSERT INTO `si_layout` VALUES ('11', 'Information');
INSERT INTO `si_layout` VALUES ('12', 'Account-No-Login');
INSERT INTO `si_layout` VALUES ('13', 'Search');
INSERT INTO `si_layout` VALUES ('14', 'Cart');

-- ----------------------------
-- Table structure for si_layout_route
-- ----------------------------
DROP TABLE IF EXISTS `si_layout_route`;
CREATE TABLE `si_layout_route` (
  `layout_route_id` int(11) NOT NULL AUTO_INCREMENT,
  `layout_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `route` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`layout_route_id`)
) ENGINE=MyISAM AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_layout_route
-- ----------------------------
INSERT INTO `si_layout_route` VALUES ('17', '10', '0', 'affiliate/');
INSERT INTO `si_layout_route` VALUES ('29', '3', '0', 'product/category');
INSERT INTO `si_layout_route` VALUES ('27', '1', '3', 'common/home');
INSERT INTO `si_layout_route` VALUES ('26', '1', '0', 'common/home');
INSERT INTO `si_layout_route` VALUES ('20', '2', '0', 'product/product');
INSERT INTO `si_layout_route` VALUES ('24', '11', '0', 'information/information');
INSERT INTO `si_layout_route` VALUES ('22', '5', '0', 'product/manufacturer');
INSERT INTO `si_layout_route` VALUES ('61', '7', '0', 'checkout/checkout');
INSERT INTO `si_layout_route` VALUES ('31', '8', '0', 'information/contact');
INSERT INTO `si_layout_route` VALUES ('32', '12', '0', 'account/logout');
INSERT INTO `si_layout_route` VALUES ('33', '12', '0', 'account/login');
INSERT INTO `si_layout_route` VALUES ('34', '12', '0', 'account/forgotten');
INSERT INTO `si_layout_route` VALUES ('35', '12', '0', 'account/register');
INSERT INTO `si_layout_route` VALUES ('58', '6', '0', 'account/address');
INSERT INTO `si_layout_route` VALUES ('57', '6', '0', 'account/invite');
INSERT INTO `si_layout_route` VALUES ('56', '6', '0', 'account/newsletter');
INSERT INTO `si_layout_route` VALUES ('55', '6', '0', 'account/transaction');
INSERT INTO `si_layout_route` VALUES ('54', '6', '0', 'account/return');
INSERT INTO `si_layout_route` VALUES ('53', '6', '0', 'account/download');
INSERT INTO `si_layout_route` VALUES ('52', '6', '0', 'account/order');
INSERT INTO `si_layout_route` VALUES ('51', '6', '0', 'account/wishlist');
INSERT INTO `si_layout_route` VALUES ('50', '6', '0', 'account/password');
INSERT INTO `si_layout_route` VALUES ('49', '6', '0', 'account/account');
INSERT INTO `si_layout_route` VALUES ('48', '6', '0', 'account/edit');
INSERT INTO `si_layout_route` VALUES ('47', '13', '0', 'product/search');
INSERT INTO `si_layout_route` VALUES ('59', '6', '0', 'account/reward');
INSERT INTO `si_layout_route` VALUES ('60', '14', '0', 'checkout/cart');

-- ----------------------------
-- Table structure for si_length_class
-- ----------------------------
DROP TABLE IF EXISTS `si_length_class`;
CREATE TABLE `si_length_class` (
  `length_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `value` decimal(15,8) NOT NULL,
  PRIMARY KEY (`length_class_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_length_class
-- ----------------------------
INSERT INTO `si_length_class` VALUES ('1', '1.00000000');
INSERT INTO `si_length_class` VALUES ('2', '10.00000000');
INSERT INTO `si_length_class` VALUES ('3', '0.39370000');

-- ----------------------------
-- Table structure for si_length_class_description
-- ----------------------------
DROP TABLE IF EXISTS `si_length_class_description`;
CREATE TABLE `si_length_class_description` (
  `length_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) COLLATE utf8_bin NOT NULL,
  `unit` varchar(4) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`length_class_id`,`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_length_class_description
-- ----------------------------
INSERT INTO `si_length_class_description` VALUES ('1', '1', '厘米', 'cm');
INSERT INTO `si_length_class_description` VALUES ('2', '1', '毫米', 'mm');
INSERT INTO `si_length_class_description` VALUES ('3', '1', '英尺', 'in');

-- ----------------------------
-- Table structure for si_logistics
-- ----------------------------
DROP TABLE IF EXISTS `si_logistics`;
CREATE TABLE `si_logistics` (
  `logistics_id` int(11) NOT NULL AUTO_INCREMENT,
  `logistics_name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logistics_link` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language_id` int(1) DEFAULT NULL,
  PRIMARY KEY (`logistics_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of si_logistics
-- ----------------------------

-- ----------------------------
-- Table structure for si_manufacturer
-- ----------------------------
DROP TABLE IF EXISTS `si_manufacturer`;
CREATE TABLE `si_manufacturer` (
  `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`manufacturer_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_manufacturer
-- ----------------------------
INSERT INTO `si_manufacturer` VALUES ('5', 'HTC', 'data/htc_logo.jpg', '0');
INSERT INTO `si_manufacturer` VALUES ('6', 'Palm', 'data/palm_logo.jpg', '0');
INSERT INTO `si_manufacturer` VALUES ('7', 'Hewlett-Packard', 'data/hp_logo.jpg', '0');
INSERT INTO `si_manufacturer` VALUES ('8', 'Apple', 'data/c-1.jpg', '0');
INSERT INTO `si_manufacturer` VALUES ('9', 'Canon', 'data/canon_logo.jpg', '0');
INSERT INTO `si_manufacturer` VALUES ('10', 'Sony', 'data/sony_logo.jpg', '0');

-- ----------------------------
-- Table structure for si_manufacturer_to_store
-- ----------------------------
DROP TABLE IF EXISTS `si_manufacturer_to_store`;
CREATE TABLE `si_manufacturer_to_store` (
  `manufacturer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`manufacturer_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_manufacturer_to_store
-- ----------------------------
INSERT INTO `si_manufacturer_to_store` VALUES ('5', '0');
INSERT INTO `si_manufacturer_to_store` VALUES ('6', '0');
INSERT INTO `si_manufacturer_to_store` VALUES ('7', '0');
INSERT INTO `si_manufacturer_to_store` VALUES ('8', '0');
INSERT INTO `si_manufacturer_to_store` VALUES ('9', '0');
INSERT INTO `si_manufacturer_to_store` VALUES ('10', '0');

-- ----------------------------
-- Table structure for si_message
-- ----------------------------
DROP TABLE IF EXISTS `si_message`;
CREATE TABLE `si_message` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `author` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `reply` text COLLATE utf8_unicode_ci,
  `status` int(1) DEFAULT '0',
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of si_message
-- ----------------------------

-- ----------------------------
-- Table structure for si_nav
-- ----------------------------
DROP TABLE IF EXISTS `si_nav`;
CREATE TABLE `si_nav` (
  `nav_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(40) COLLATE utf8_bin NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `url` varchar(200) COLLATE utf8_bin NOT NULL,
  `tid` varchar(20) COLLATE utf8_bin NOT NULL,
  `ishome` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`nav_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_nav
-- ----------------------------
INSERT INTO `si_nav` VALUES ('1', '首页', '5', '1', 'index.php?route=common/home', 'home', '1');
INSERT INTO `si_nav` VALUES ('2', '电子菜单', '4', '1', 'index.php?route=product/category&amp;path=0', 'category', '1');
INSERT INTO `si_nav` VALUES ('3', '会员中心', '3', '1', 'index.php?route=account/account', 'account', '1');
INSERT INTO `si_nav` VALUES ('4', '留言板', '2', '1', 'index.php?route=information/contact', 'contact', '1');

-- ----------------------------
-- Table structure for si_option
-- ----------------------------
DROP TABLE IF EXISTS `si_option`;
CREATE TABLE `si_option` (
  `option_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) COLLATE utf8_bin NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`option_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_option
-- ----------------------------
INSERT INTO `si_option` VALUES ('2', 'checkbox', '3');
INSERT INTO `si_option` VALUES ('7', 'file', '6');
INSERT INTO `si_option` VALUES ('8', 'date', '7');
INSERT INTO `si_option` VALUES ('10', 'datetime', '9');
INSERT INTO `si_option` VALUES ('11', 'select', '1');
INSERT INTO `si_option` VALUES ('12', 'date', '1');
INSERT INTO `si_option` VALUES ('13', 'color', '1');
INSERT INTO `si_option` VALUES ('14', 'virtual_product', '11');
INSERT INTO `si_option` VALUES ('15', 'color', '1');

-- ----------------------------
-- Table structure for si_option_description
-- ----------------------------
DROP TABLE IF EXISTS `si_option_description`;
CREATE TABLE `si_option_description` (
  `option_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`option_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_option_description
-- ----------------------------
INSERT INTO `si_option_description` VALUES ('2', '1', '复选项');
INSERT INTO `si_option_description` VALUES ('8', '1', '日期');
INSERT INTO `si_option_description` VALUES ('7', '1', '上传文件');
INSERT INTO `si_option_description` VALUES ('10', '1', '日期和时间');
INSERT INTO `si_option_description` VALUES ('12', '1', '送达日期');
INSERT INTO `si_option_description` VALUES ('11', '1', '大小');
INSERT INTO `si_option_description` VALUES ('15', '1', '颜色');
INSERT INTO `si_option_description` VALUES ('14', '1', '点卡');

-- ----------------------------
-- Table structure for si_option_value
-- ----------------------------
DROP TABLE IF EXISTS `si_option_value`;
CREATE TABLE `si_option_value` (
  `option_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`option_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_option_value
-- ----------------------------

-- ----------------------------
-- Table structure for si_option_value_description
-- ----------------------------
DROP TABLE IF EXISTS `si_option_value_description`;
CREATE TABLE `si_option_value_description` (
  `option_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`option_value_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_option_value_description
-- ----------------------------

-- ----------------------------
-- Table structure for si_order
-- ----------------------------
DROP TABLE IF EXISTS `si_order`;
CREATE TABLE `si_order` (
  `order_id` varchar(128) COLLATE utf8_bin NOT NULL,
  `invoice_no` int(11) NOT NULL DEFAULT '0',
  `invoice_prefix` varchar(10) COLLATE utf8_bin NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `store_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `store_url` varchar(255) COLLATE utf8_bin NOT NULL,
  `type` int(11) DEFAULT '1',
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `lastname` varchar(32) COLLATE utf8_bin NOT NULL,
  `email` varchar(96) COLLATE utf8_bin NOT NULL,
  `telephone` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `seat` int(11) DEFAULT NULL,
  `fax` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `shipping_firstname` varchar(32) COLLATE utf8_bin NOT NULL,
  `shipping_lastname` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `shipping_company` varchar(32) COLLATE utf8_bin NOT NULL,
  `shipping_address_1` varchar(128) COLLATE utf8_bin NOT NULL,
  `shipping_address_2` varchar(128) COLLATE utf8_bin NOT NULL,
  `shipping_city` varchar(128) COLLATE utf8_bin NOT NULL,
  `shipping_city_id` int(11) NOT NULL,
  `shipping_postcode` varchar(10) COLLATE utf8_bin NOT NULL DEFAULT '',
  `shipping_mobile` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `shipping_phone` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `shipping_country` varchar(128) COLLATE utf8_bin NOT NULL,
  `shipping_country_id` int(11) NOT NULL,
  `shipping_zone` varchar(128) COLLATE utf8_bin NOT NULL,
  `shipping_zone_id` int(11) NOT NULL,
  `shipping_address_format` text COLLATE utf8_bin NOT NULL,
  `shipping_method` varchar(128) COLLATE utf8_bin NOT NULL DEFAULT '',
  `payment_firstname` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `payment_lastname` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `payment_company` varchar(32) COLLATE utf8_bin NOT NULL,
  `payment_address_1` varchar(128) COLLATE utf8_bin NOT NULL,
  `payment_address_2` varchar(128) COLLATE utf8_bin NOT NULL,
  `payment_city` varchar(128) COLLATE utf8_bin NOT NULL,
  `payment_city_id` int(11) NOT NULL,
  `payment_postcode` varchar(10) COLLATE utf8_bin NOT NULL DEFAULT '',
  `payment_country` varchar(128) COLLATE utf8_bin NOT NULL,
  `payment_country_id` int(11) NOT NULL,
  `payment_zone` varchar(128) COLLATE utf8_bin NOT NULL,
  `payment_zone_id` int(11) NOT NULL,
  `payment_address_format` text COLLATE utf8_bin NOT NULL,
  `payment_method` varchar(128) COLLATE utf8_bin NOT NULL DEFAULT '',
  `comment` text COLLATE utf8_bin NOT NULL,
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `reward` int(8) NOT NULL,
  `order_status_id` int(11) NOT NULL DEFAULT '0',
  `affiliate_id` int(11) NOT NULL,
  `commission` decimal(15,4) NOT NULL,
  `language_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `currency_code` varchar(3) COLLATE utf8_bin NOT NULL,
  `currency_value` decimal(15,8) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `ip` varchar(15) COLLATE utf8_bin NOT NULL DEFAULT '',
  `express` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `express_website` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `express_no` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `payment_code` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `city_id` int(11) NOT NULL DEFAULT '0',
  `address` varchar(10) COLLATE utf8_bin NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_order
-- ----------------------------

-- ----------------------------
-- Table structure for si_order_download
-- ----------------------------
DROP TABLE IF EXISTS `si_order_download`;
CREATE TABLE `si_order_download` (
  `order_download_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(128) COLLATE utf8_bin NOT NULL,
  `order_product_id` int(11) NOT NULL,
  `name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `filename` varchar(128) COLLATE utf8_bin NOT NULL DEFAULT '',
  `mask` varchar(128) COLLATE utf8_bin NOT NULL DEFAULT '',
  `remaining` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_download_id`)
) ENGINE=MyISAM AUTO_INCREMENT=307 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_order_download
-- ----------------------------

-- ----------------------------
-- Table structure for si_order_history
-- ----------------------------
DROP TABLE IF EXISTS `si_order_history`;
CREATE TABLE `si_order_history` (
  `order_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(128) COLLATE utf8_bin NOT NULL,
  `order_status_id` int(5) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `comment` text COLLATE utf8_bin NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`order_history_id`)
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_order_history
-- ----------------------------

-- ----------------------------
-- Table structure for si_order_option
-- ----------------------------
DROP TABLE IF EXISTS `si_order_option`;
CREATE TABLE `si_order_option` (
  `order_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(128) COLLATE utf8_bin NOT NULL,
  `order_product_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_option_value_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `value` text COLLATE utf8_bin NOT NULL,
  `type` varchar(32) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`order_option_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_order_option
-- ----------------------------

-- ----------------------------
-- Table structure for si_order_product
-- ----------------------------
DROP TABLE IF EXISTS `si_order_product`;
CREATE TABLE `si_order_product` (
  `order_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(128) COLLATE utf8_bin NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `model` varchar(24) COLLATE utf8_bin NOT NULL,
  `quantity` int(4) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `tax` decimal(15,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`order_product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_order_product
-- ----------------------------

-- ----------------------------
-- Table structure for si_order_status
-- ----------------------------
DROP TABLE IF EXISTS `si_order_status`;
CREATE TABLE `si_order_status` (
  `order_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`order_status_id`,`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_order_status
-- ----------------------------
INSERT INTO `si_order_status` VALUES ('2', '1', '配送中');
INSERT INTO `si_order_status` VALUES ('3', '1', '已发货');
INSERT INTO `si_order_status` VALUES ('7', '1', '取消订单');
INSERT INTO `si_order_status` VALUES ('5', '1', '完成');
INSERT INTO `si_order_status` VALUES ('8', '1', '被拒绝');
INSERT INTO `si_order_status` VALUES ('10', '1', '失败');
INSERT INTO `si_order_status` VALUES ('11', '1', '退款');
INSERT INTO `si_order_status` VALUES ('13', '1', '扣款');
INSERT INTO `si_order_status` VALUES ('1', '1', '待处理');
INSERT INTO `si_order_status` VALUES ('15', '1', '已处理');
INSERT INTO `si_order_status` VALUES ('14', '1', '过期');
INSERT INTO `si_order_status` VALUES ('16', '1', '待付款');

-- ----------------------------
-- Table structure for si_order_total
-- ----------------------------
DROP TABLE IF EXISTS `si_order_total`;
CREATE TABLE `si_order_total` (
  `order_total_id` int(10) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(128) COLLATE utf8_bin NOT NULL,
  `code` varchar(32) COLLATE utf8_bin NOT NULL,
  `title` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `text` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `value` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`order_total_id`),
  KEY `idx_orders_total_orders_id` (`order_id`)
) ENGINE=MyISAM AUTO_INCREMENT=81 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_order_total
-- ----------------------------

-- ----------------------------
-- Table structure for si_product
-- ----------------------------
DROP TABLE IF EXISTS `si_product`;
CREATE TABLE `si_product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(64) COLLATE utf8_bin NOT NULL,
  `sku` varchar(64) COLLATE utf8_bin NOT NULL,
  `upc` varchar(12) COLLATE utf8_bin NOT NULL,
  `location` varchar(128) COLLATE utf8_bin NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `unit_id` int(11) NOT NULL,
  `stock_status_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `shipping` tinyint(1) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `points` int(8) NOT NULL DEFAULT '0',
  `tax_class_id` int(11) NOT NULL,
  `date_available` date NOT NULL,
  `weight` decimal(5,2) NOT NULL DEFAULT '0.00',
  `weight_class_id` int(11) NOT NULL DEFAULT '0',
  `length` decimal(5,2) NOT NULL DEFAULT '0.00',
  `width` decimal(5,2) NOT NULL DEFAULT '0.00',
  `height` decimal(5,2) NOT NULL DEFAULT '0.00',
  `length_class_id` int(11) NOT NULL DEFAULT '0',
  `subtract` tinyint(1) NOT NULL DEFAULT '1',
  `minimum` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `viewed` int(5) NOT NULL DEFAULT '0',
  `tag` varchar(10) COLLATE utf8_bin NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_product
-- ----------------------------

-- ----------------------------
-- Table structure for si_product_attribute
-- ----------------------------
DROP TABLE IF EXISTS `si_product_attribute`;
CREATE TABLE `si_product_attribute` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `text` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`product_id`,`attribute_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_product_attribute
-- ----------------------------

-- ----------------------------
-- Table structure for si_product_description
-- ----------------------------
DROP TABLE IF EXISTS `si_product_description`;
CREATE TABLE `si_product_description` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_bin NOT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`product_id`,`language_id`),
  KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=55192 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_product_description
-- ----------------------------

-- ----------------------------
-- Table structure for si_product_discount
-- ----------------------------
DROP TABLE IF EXISTS `si_product_discount`;
CREATE TABLE `si_product_discount` (
  `product_discount_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`product_discount_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=70610 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_product_discount
-- ----------------------------

-- ----------------------------
-- Table structure for si_product_image
-- ----------------------------
DROP TABLE IF EXISTS `si_product_image`;
CREATE TABLE `si_product_image` (
  `product_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`product_image_id`)
) ENGINE=MyISAM AUTO_INCREMENT=143032 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_product_image
-- ----------------------------

-- ----------------------------
-- Table structure for si_product_option
-- ----------------------------
DROP TABLE IF EXISTS `si_product_option`;
CREATE TABLE `si_product_option` (
  `product_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value` text COLLATE utf8_bin NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_option_id`)
) ENGINE=MyISAM AUTO_INCREMENT=90464 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_product_option
-- ----------------------------

-- ----------------------------
-- Table structure for si_product_option_value
-- ----------------------------
DROP TABLE IF EXISTS `si_product_option_value`;
CREATE TABLE `si_product_option_value` (
  `product_option_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL,
  `quantity` int(3) NOT NULL,
  `subtract` tinyint(1) NOT NULL DEFAULT '0',
  `price` decimal(15,4) NOT NULL,
  `price_prefix` varchar(1) COLLATE utf8_bin NOT NULL,
  `points` int(8) NOT NULL,
  `points_prefix` varchar(1) COLLATE utf8_bin NOT NULL,
  `weight` decimal(15,8) NOT NULL,
  `weight_prefix` varchar(1) COLLATE utf8_bin NOT NULL,
  `color_product_id` int(11) DEFAULT '0',
  `product_value` tinytext COLLATE utf8_bin,
  PRIMARY KEY (`product_option_value_id`)
) ENGINE=MyISAM AUTO_INCREMENT=215530 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_product_option_value
-- ----------------------------

-- ----------------------------
-- Table structure for si_product_related
-- ----------------------------
DROP TABLE IF EXISTS `si_product_related`;
CREATE TABLE `si_product_related` (
  `product_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`related_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_product_related
-- ----------------------------

-- ----------------------------
-- Table structure for si_product_reward
-- ----------------------------
DROP TABLE IF EXISTS `si_product_reward`;
CREATE TABLE `si_product_reward` (
  `product_reward_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `points` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_reward_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_product_reward
-- ----------------------------

-- ----------------------------
-- Table structure for si_product_special
-- ----------------------------
DROP TABLE IF EXISTS `si_product_special`;
CREATE TABLE `si_product_special` (
  `product_special_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`product_special_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_product_special
-- ----------------------------

-- ----------------------------
-- Table structure for si_product_tag
-- ----------------------------
DROP TABLE IF EXISTS `si_product_tag`;
CREATE TABLE `si_product_tag` (
  `product_tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `tag` varchar(32) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`product_tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_product_tag
-- ----------------------------

-- ----------------------------
-- Table structure for si_product_to_category
-- ----------------------------
DROP TABLE IF EXISTS `si_product_to_category`;
CREATE TABLE `si_product_to_category` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_product_to_category
-- ----------------------------

-- ----------------------------
-- Table structure for si_product_to_download
-- ----------------------------
DROP TABLE IF EXISTS `si_product_to_download`;
CREATE TABLE `si_product_to_download` (
  `product_id` int(11) NOT NULL,
  `download_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`download_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_product_to_download
-- ----------------------------

-- ----------------------------
-- Table structure for si_product_to_layout
-- ----------------------------
DROP TABLE IF EXISTS `si_product_to_layout`;
CREATE TABLE `si_product_to_layout` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_product_to_layout
-- ----------------------------

-- ----------------------------
-- Table structure for si_product_to_store
-- ----------------------------
DROP TABLE IF EXISTS `si_product_to_store`;
CREATE TABLE `si_product_to_store` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_product_to_store
-- ----------------------------

-- ----------------------------
-- Table structure for si_return_action
-- ----------------------------
DROP TABLE IF EXISTS `si_return_action`;
CREATE TABLE `si_return_action` (
  `return_action_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`return_action_id`,`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_return_action
-- ----------------------------
INSERT INTO `si_return_action` VALUES ('1', '1', '已经退款');
INSERT INTO `si_return_action` VALUES ('2', '1', '积分问题');
INSERT INTO `si_return_action` VALUES ('3', '1', '已换货');

-- ----------------------------
-- Table structure for si_return_history
-- ----------------------------
DROP TABLE IF EXISTS `si_return_history`;
CREATE TABLE `si_return_history` (
  `return_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `return_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `comment` text COLLATE utf8_bin NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`return_history_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_return_history
-- ----------------------------

-- ----------------------------
-- Table structure for si_return_product
-- ----------------------------
DROP TABLE IF EXISTS `si_return_product`;
CREATE TABLE `si_return_product` (
  `return_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `return_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `model` varchar(64) COLLATE utf8_bin NOT NULL,
  `quantity` int(4) NOT NULL,
  `return_reason_id` int(11) NOT NULL,
  `opened` tinyint(1) NOT NULL DEFAULT '0',
  `comment` text COLLATE utf8_bin NOT NULL,
  `return_action_id` int(11) NOT NULL,
  PRIMARY KEY (`return_product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_return_product
-- ----------------------------

-- ----------------------------
-- Table structure for si_return_reason
-- ----------------------------
DROP TABLE IF EXISTS `si_return_reason`;
CREATE TABLE `si_return_reason` (
  `return_reason_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(128) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`return_reason_id`,`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_return_reason
-- ----------------------------
INSERT INTO `si_return_reason` VALUES ('1', '1', '送货太慢');
INSERT INTO `si_return_reason` VALUES ('2', '1', '收到的货品不对');
INSERT INTO `si_return_reason` VALUES ('3', '1', '订单下错了');
INSERT INTO `si_return_reason` VALUES ('4', '1', '货品有瑕疵');
INSERT INTO `si_return_reason` VALUES ('5', '1', '其他原因');

-- ----------------------------
-- Table structure for si_return_status
-- ----------------------------
DROP TABLE IF EXISTS `si_return_status`;
CREATE TABLE `si_return_status` (
  `return_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`return_status_id`,`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_return_status
-- ----------------------------
INSERT INTO `si_return_status` VALUES ('1', '1', '待处理');
INSERT INTO `si_return_status` VALUES ('3', '1', '完成');
INSERT INTO `si_return_status` VALUES ('2', '1', '等待发货');

-- ----------------------------
-- Table structure for si_review
-- ----------------------------
DROP TABLE IF EXISTS `si_review`;
CREATE TABLE `si_review` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `text` text COLLATE utf8_bin NOT NULL,
  `rating` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`review_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_review
-- ----------------------------

-- ----------------------------
-- Table structure for si_setting
-- ----------------------------
DROP TABLE IF EXISTS `si_setting`;
CREATE TABLE `si_setting` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `group` varchar(32) COLLATE utf8_bin NOT NULL,
  `key` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `value` text COLLATE utf8_bin NOT NULL,
  `serialized` tinyint(1) NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=MyISAM AUTO_INCREMENT=31252 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_setting
-- ----------------------------
INSERT INTO `si_setting` VALUES ('21122', '0', 'shipping', 'shipping_sort_order', 0x31, '0');
INSERT INTO `si_setting` VALUES ('3453', '0', 'sub_total', 'sub_total_sort_order', 0x31, '0');
INSERT INTO `si_setting` VALUES ('3452', '0', 'sub_total', 'sub_total_status', 0x31, '0');
INSERT INTO `si_setting` VALUES ('272', '0', 'tax', 'tax_status', 0x31, '0');
INSERT INTO `si_setting` VALUES ('16013', '0', 'total', 'total_sort_order', 0x39, '0');
INSERT INTO `si_setting` VALUES ('16012', '0', 'total', 'total_status', 0x31, '0');
INSERT INTO `si_setting` VALUES ('273', '0', 'tax', 'tax_sort_order', 0x35, '0');
INSERT INTO `si_setting` VALUES ('31222', '0', 'config', 'config_sms_notice', 0x30, '0');
INSERT INTO `si_setting` VALUES ('26665', '0', 'cod', 'cod_status', 0x31, '0');
INSERT INTO `si_setting` VALUES ('21121', '0', 'shipping', 'shipping_status', 0x31, '0');
INSERT INTO `si_setting` VALUES ('21120', '0', 'shipping', 'shipping_estimator', 0x30, '0');
INSERT INTO `si_setting` VALUES ('13801', '0', 'coupon', 'coupon_sort_order', 0x34, '0');
INSERT INTO `si_setting` VALUES ('13800', '0', 'coupon', 'coupon_status', 0x31, '0');
INSERT INTO `si_setting` VALUES ('31224', '0', 'config', 'config_distribution', 0x30, '0');
INSERT INTO `si_setting` VALUES ('31202', '0', 'config', 'config_name', 0xE5A1ABE58699E5908DE7A7B0, '0');
INSERT INTO `si_setting` VALUES ('30044', '0', 'config', 'config_store_status', 0x31, '0');
INSERT INTO `si_setting` VALUES ('26364', '0', 'carousel', 'carousel_module', 0x613A313A7B693A303B613A393A7B733A393A2262616E6E65725F6964223B733A323A223131223B733A353A226C696D6974223B733A313A2235223B733A363A227363726F6C6C223B733A313A2233223B733A353A227769647468223B733A323A223830223B733A363A22686569676874223B733A323A223830223B733A393A226C61796F75745F6964223B733A313A2231223B733A383A22706F736974696F6E223B733A31343A22636F6E74656E745F626F74746F6D223B733A363A22737461747573223B733A313A2231223B733A31303A22736F72745F6F72646572223B733A313A2233223B7D7D, '1');
INSERT INTO `si_setting` VALUES ('29957', '0', 'config', 'config_image_manufacturer_height', 0x323430, '0');
INSERT INTO `si_setting` VALUES ('29965', '0', 'config', 'config_image_wishlist_height', 0x3830, '0');
INSERT INTO `si_setting` VALUES ('9444', '0', 'credit', 'credit_sort_order', 0x37, '0');
INSERT INTO `si_setting` VALUES ('9443', '0', 'credit', 'credit_status', 0x31, '0');
INSERT INTO `si_setting` VALUES ('18432', '0', 'reward', 'reward_sort_order', 0x32, '0');
INSERT INTO `si_setting` VALUES ('18431', '0', 'reward', 'reward_status', 0x31, '0');
INSERT INTO `si_setting` VALUES ('25516', '0', 'affiliate', 'affiliate_module', 0x613A313A7B693A303B613A343A7B733A393A226C61796F75745F6964223B733A323A223130223B733A383A22706F736974696F6E223B733A31313A22636F6C756D6E5F6C656674223B733A363A22737461747573223B733A313A2231223B733A31303A22736F72745F6F72646572223B733A313A2231223B7D7D, '1');
INSERT INTO `si_setting` VALUES ('28160', '0', 'google_sitemap', 'google_sitemap_status', 0x31, '0');
INSERT INTO `si_setting` VALUES ('31218', '0', 'config', 'config_hoursfrom', 0x39, '0');
INSERT INTO `si_setting` VALUES ('24385', '0', 'slideshow', 'slideshow_module', 0x613A313A7B693A303B613A373A7B733A393A2262616E6E65725F6964223B733A313A2239223B733A353A227769647468223B733A333A22393630223B733A363A22686569676874223B733A333A22323830223B733A393A226C61796F75745F6964223B733A313A2231223B733A383A22706F736974696F6E223B733A31313A22636F6E74656E745F746F70223B733A363A22737461747573223B733A313A2231223B733A31303A22736F72745F6F72646572223B733A313A2230223B7D7D, '1');
INSERT INTO `si_setting` VALUES ('29966', '0', 'config', 'config_image_cart_width', 0x3830, '0');
INSERT INTO `si_setting` VALUES ('25466', '0', 'account', 'account_module', 0x613A313A7B693A303B613A343A7B733A393A226C61796F75745F6964223B733A313A2236223B733A383A22706F736974696F6E223B733A31313A22636F6C756D6E5F6C656674223B733A363A22737461747573223B733A313A2231223B733A31303A22736F72745F6F72646572223B733A313A2231223B7D7D, '1');
INSERT INTO `si_setting` VALUES ('29964', '0', 'config', 'config_image_wishlist_width', 0x3830, '0');
INSERT INTO `si_setting` VALUES ('29963', '0', 'config', 'config_image_compare_height', 0x313230, '0');
INSERT INTO `si_setting` VALUES ('29962', '0', 'config', 'config_image_compare_width', 0x3930, '0');
INSERT INTO `si_setting` VALUES ('29961', '0', 'config', 'config_image_related_height', 0x313230, '0');
INSERT INTO `si_setting` VALUES ('29960', '0', 'config', 'config_image_related_width', 0x3930, '0');
INSERT INTO `si_setting` VALUES ('29959', '0', 'config', 'config_image_additional_height', 0x323530, '0');
INSERT INTO `si_setting` VALUES ('29958', '0', 'config', 'config_image_additional_width', 0x323530, '0');
INSERT INTO `si_setting` VALUES ('29956', '0', 'config', 'config_image_manufacturer_width', 0x313830, '0');
INSERT INTO `si_setting` VALUES ('29955', '0', 'config', 'config_image_category_height', 0x313830, '0');
INSERT INTO `si_setting` VALUES ('29954', '0', 'config', 'config_image_category_width', 0x313830, '0');
INSERT INTO `si_setting` VALUES ('29953', '0', 'config', 'config_image_product_height', 0x313030, '0');
INSERT INTO `si_setting` VALUES ('29952', '0', 'config', 'config_image_product_width', 0x313030, '0');
INSERT INTO `si_setting` VALUES ('29951', '0', 'config', 'config_image_popup_height', 0x31363030, '0');
INSERT INTO `si_setting` VALUES ('29950', '0', 'config', 'config_image_popup_width', 0x31323030, '0');
INSERT INTO `si_setting` VALUES ('29949', '0', 'config', 'config_image_thumb_height', 0x323530, '0');
INSERT INTO `si_setting` VALUES ('29948', '0', 'config', 'config_image_thumb_width', 0x323530, '0');
INSERT INTO `si_setting` VALUES ('26726', '0', 'config', 'config_cart_weight', 0x30, '0');
INSERT INTO `si_setting` VALUES ('26725', '0', 'config', 'config_upload_allowed', 0x6A70672C204A50472C206A7065672C206769662C20706E672C20747874, '0');
INSERT INTO `si_setting` VALUES ('26724', '0', 'config', 'config_download', 0x31, '0');
INSERT INTO `si_setting` VALUES ('30839', '0', 'config', 'config_review_status', 0x30, '0');
INSERT INTO `si_setting` VALUES ('30845', '0', 'config', 'config_return_status_id', 0x31, '0');
INSERT INTO `si_setting` VALUES ('30844', '0', 'config', 'config_complete_status_id', 0x35, '0');
INSERT INTO `si_setting` VALUES ('31232', '0', 'config', 'config_order_status_id', 0x31, '0');
INSERT INTO `si_setting` VALUES ('30842', '0', 'config', 'config_stock_status_id', 0x37, '0');
INSERT INTO `si_setting` VALUES ('31231', '0', 'config', 'config_stock_checkout', 0x30, '0');
INSERT INTO `si_setting` VALUES ('30843', '0', 'config', 'config_stock_warning', 0x30, '0');
INSERT INTO `si_setting` VALUES ('31230', '0', 'config', 'config_stock_display', 0x30, '0');
INSERT INTO `si_setting` VALUES ('26719', '0', 'config', 'config_commission', 0x35, '0');
INSERT INTO `si_setting` VALUES ('26718', '0', 'config', 'config_affiliate_id', 0x35, '0');
INSERT INTO `si_setting` VALUES ('31229', '0', 'config', 'config_checkout_id', 0x33, '0');
INSERT INTO `si_setting` VALUES ('31228', '0', 'config', 'config_account_id', 0x35, '0');
INSERT INTO `si_setting` VALUES ('31233', '0', 'config', 'config_guest_checkout', 0x30, '0');
INSERT INTO `si_setting` VALUES ('31227', '0', 'config', 'config_customer_approval', 0x30, '0');
INSERT INTO `si_setting` VALUES ('28694', '0', 'config', 'config_customer_price', 0x30, '0');
INSERT INTO `si_setting` VALUES ('31226', '0', 'config', 'config_customer_group_id', 0x38, '0');
INSERT INTO `si_setting` VALUES ('30834', '0', 'config', 'config_invoice_prefix', 0x474F4F442F303031, '0');
INSERT INTO `si_setting` VALUES ('31225', '0', 'config', 'config_tax', 0x30, '0');
INSERT INTO `si_setting` VALUES ('30832', '0', 'config', 'config_admin_limit', 0x3430, '0');
INSERT INTO `si_setting` VALUES ('31216', '0', 'config', 'config_catalog_limit', 0x37, '0');
INSERT INTO `si_setting` VALUES ('26717', '0', 'config', 'config_weight_class_id', 0x31, '0');
INSERT INTO `si_setting` VALUES ('26716', '0', 'config', 'config_length_class_id', 0x31, '0');
INSERT INTO `si_setting` VALUES ('24038', '0', 'config', 'config_currency_auto', 0x31, '0');
INSERT INTO `si_setting` VALUES ('31215', '0', 'config', 'config_currency', 0x434E59, '0');
INSERT INTO `si_setting` VALUES ('31213', '0', 'config', 'config_zone_id', '', '0');
INSERT INTO `si_setting` VALUES ('31214', '0', 'config', 'config_language', 0x636E, '0');
INSERT INTO `si_setting` VALUES ('30841', '0', 'config', 'config_admin_language', 0x636E, '0');
INSERT INTO `si_setting` VALUES ('31212', '0', 'config', 'config_country_id', 0x3434, '0');
INSERT INTO `si_setting` VALUES ('28686', '0', 'config', 'config_layout_id', 0x34, '0');
INSERT INTO `si_setting` VALUES ('31211', '0', 'config', 'config_template', 0x6469616E63616E, '0');
INSERT INTO `si_setting` VALUES ('31209', '0', 'config', 'config_meta_description', 0xE5BA97E993BAE5908DE7A7B0, '0');
INSERT INTO `si_setting` VALUES ('31208', '0', 'config', 'config_title', 0xE5BA97E993BAE5908DE7A7B0, '0');
INSERT INTO `si_setting` VALUES ('28681', '0', 'config', 'config_fax', '', '0');
INSERT INTO `si_setting` VALUES ('31207', '0', 'config', 'config_telephone', 0x313233343536373839, '0');
INSERT INTO `si_setting` VALUES ('31203', '0', 'config', 'config_owner', 0xE5A1ABE58699E5BA97E4B8BB, '0');
INSERT INTO `si_setting` VALUES ('29967', '0', 'config', 'config_image_cart_height', 0x3830, '0');
INSERT INTO `si_setting` VALUES ('31118', '0', 'config', 'config_use_ssl', 0x30, '0');
INSERT INTO `si_setting` VALUES ('31119', '0', 'config', 'config_seo_url', 0x30, '0');
INSERT INTO `si_setting` VALUES ('31120', '0', 'config', 'config_maintenance', 0x30, '0');
INSERT INTO `si_setting` VALUES ('31121', '0', 'config', 'config_encryption', 0x3132333435, '0');
INSERT INTO `si_setting` VALUES ('31122', '0', 'config', 'config_compression', 0x30, '0');
INSERT INTO `si_setting` VALUES ('31124', '0', 'config', 'config_error_display', 0x30, '0');
INSERT INTO `si_setting` VALUES ('31125', '0', 'config', 'config_error_log', 0x30, '0');
INSERT INTO `si_setting` VALUES ('31126', '0', 'config', 'config_error_filename', 0x6572726F722E747874, '0');
INSERT INTO `si_setting` VALUES ('31127', '0', 'config', 'config_google_analytics', '', '0');
INSERT INTO `si_setting` VALUES ('31247', '0', 'config', 'config_smtp_username', 0x3132334071712E636F6D, '0');
INSERT INTO `si_setting` VALUES ('31249', '0', 'config', 'config_smtp_port', 0x3235, '0');
INSERT INTO `si_setting` VALUES ('31250', '0', 'config', 'config_smtp_timeout', 0x35, '0');
INSERT INTO `si_setting` VALUES ('31251', '0', 'config', 'config_alert_mail', 0x30, '0');
INSERT INTO `si_setting` VALUES ('31115', '0', 'config', 'config_account_mail', 0x30, '0');
INSERT INTO `si_setting` VALUES ('31116', '0', 'config', 'config_alert_emails', '', '0');
INSERT INTO `si_setting` VALUES ('26387', '0', 'latest', 'latest_module', 0x613A313A7B693A303B613A373A7B733A353A226C696D6974223B733A313A2234223B733A31313A22696D6167655F7769647468223B733A333A22323330223B733A31323A22696D6167655F686569676874223B733A333A22333030223B733A393A226C61796F75745F6964223B733A313A2231223B733A383A22706F736974696F6E223B733A31313A22636F6E74656E745F746F70223B733A363A22737461747573223B733A313A2231223B733A31303A22736F72745F6F72646572223B733A313A2231223B7D7D, '1');
INSERT INTO `si_setting` VALUES ('26711', '0', 'config', 'config_active', 0x30, '0');
INSERT INTO `si_setting` VALUES ('28701', '0', 'config', 'config_order_nopay_status_id', 0x3136, '0');
INSERT INTO `si_setting` VALUES ('30840', '0', 'config', 'config_invite_points', 0x313030, '0');
INSERT INTO `si_setting` VALUES ('25033', '0', 'banner', 'banner_module', 0x613A333A7B693A303B613A373A7B733A393A2262616E6E65725F6964223B733A323A223130223B733A353A227769647468223B733A333A22313832223B733A363A22686569676874223B733A333A22313832223B733A393A226C61796F75745F6964223B733A313A2233223B733A383A22706F736974696F6E223B733A31313A22636F6C756D6E5F6C656674223B733A363A22737461747573223B733A313A2231223B733A31303A22736F72745F6F72646572223B733A313A2233223B7D693A313B613A373A7B733A393A2262616E6E65725F6964223B733A323A223130223B733A353A227769647468223B733A333A22393630223B733A363A22686569676874223B733A333A22313335223B733A393A226C61796F75745F6964223B733A313A2231223B733A383A22706F736974696F6E223B733A31313A22636F6E74656E745F746F70223B733A363A22737461747573223B733A313A2231223B733A31303A22736F72745F6F72646572223B733A313A2232223B7D693A323B613A373A7B733A393A2262616E6E65725F6964223B733A323A223132223B733A353A227769647468223B733A333A22373730223B733A363A22686569676874223B733A333A22313430223B733A393A226C61796F75745F6964223B733A313A2233223B733A383A22706F736974696F6E223B733A31343A22636F6E74656E745F626F74746F6D223B733A363A22737461747573223B733A313A2231223B733A31303A22736F72745F6F72646572223B733A313A2233223B7D7D, '1');
INSERT INTO `si_setting` VALUES ('31204', '0', 'config', 'config_address', 0xE5BA97E993BAE59CB0E59D80, '0');
INSERT INTO `si_setting` VALUES ('31206', '0', 'config', 'config_email', 0x333934353232324071712E636F6D, '0');
INSERT INTO `si_setting` VALUES ('31210', '0', 'config', 'config_meta_keyword', 0xE5BA97E993BAE5908DE7A7B0, '0');
INSERT INTO `si_setting` VALUES ('31236', '0', 'config', 'config_color_css', '', '0');
INSERT INTO `si_setting` VALUES ('31237', '0', 'config', 'config_home_image_s', 0x30, '0');
INSERT INTO `si_setting` VALUES ('29974', '0', 'weight', 'weight_18_status', 0x31, '0');
INSERT INTO `si_setting` VALUES ('29973', '0', 'weight', 'weight_18_time', 0x3334, '0');
INSERT INTO `si_setting` VALUES ('29972', '0', 'weight', 'weight_18_rate', 0x3130, '0');
INSERT INTO `si_setting` VALUES ('29971', '0', 'weight', 'weight_sort_order', '', '0');
INSERT INTO `si_setting` VALUES ('29970', '0', 'weight', 'weight_description', '', '0');
INSERT INTO `si_setting` VALUES ('29969', '0', 'weight', 'weight_status', 0x31, '0');
INSERT INTO `si_setting` VALUES ('29968', '0', 'weight', 'weight_tax_class_id', 0x30, '0');
INSERT INTO `si_setting` VALUES ('26664', '0', 'cod', 'cod_geo_zone_id', 0x30, '0');
INSERT INTO `si_setting` VALUES ('25914', '0', 'category', 'category_module', 0x613A333A7B693A303B613A343A7B733A393A226C61796F75745F6964223B733A313A2233223B733A383A22706F736974696F6E223B733A31313A22636F6C756D6E5F6C656674223B733A363A22737461747573223B733A313A2231223B733A31303A22736F72745F6F72646572223B733A313A2231223B7D693A313B613A343A7B733A393A226C61796F75745F6964223B733A313A2232223B733A383A22706F736974696F6E223B733A31313A22636F6C756D6E5F6C656674223B733A363A22737461747573223B733A313A2231223B733A31303A22736F72745F6F72646572223B733A313A2231223B7D693A323B613A343A7B733A393A226C61796F75745F6964223B733A323A223133223B733A383A22706F736974696F6E223B733A31313A22636F6C756D6E5F6C656674223B733A363A22737461747573223B733A313A2231223B733A31303A22736F72745F6F72646572223B733A313A2231223B7D7D, '1');
INSERT INTO `si_setting` VALUES ('31219', '0', 'config', 'config_hoursto', 0x3232, '0');
INSERT INTO `si_setting` VALUES ('26663', '0', 'cod', 'cod_order_status_id', 0x3133, '0');
INSERT INTO `si_setting` VALUES ('28528', '0', 'config', 'config_default_payment', 0x636F64, '0');
INSERT INTO `si_setting` VALUES ('29947', '0', 'config', 'config_icon', '', '0');
INSERT INTO `si_setting` VALUES ('30838', '0', 'config', 'config_review', 0x30, '0');
INSERT INTO `si_setting` VALUES ('26268', '0', 'google_talk', 'google_talk_module', 0x613A313A7B693A303B613A343A7B733A393A226C61796F75745F6964223B733A313A2232223B733A383A22706F736974696F6E223B733A31313A22636F6C756D6E5F6C656674223B733A363A22737461747573223B733A313A2231223B733A31303A22736F72745F6F72646572223B733A313A2230223B7D7D, '1');
INSERT INTO `si_setting` VALUES ('26480', '0', 'onlineim', 'onlineim_module', 0x613A313A7B693A303B613A343A7B733A393A226C61796F75745F6964223B733A313A2232223B733A383A22706F736974696F6E223B733A31313A22636F6C756D6E5F6C656674223B733A363A22737461747573223B733A313A2231223B733A31303A22736F72745F6F72646572223B733A313A2230223B7D7D, '1');
INSERT INTO `si_setting` VALUES ('26324', '0', 'google_base', 'google_base_status', 0x31, '0');
INSERT INTO `si_setting` VALUES ('26331', '0', 'dealday', 'dealday_module', 0x613A313A7B693A303B613A373A7B733A353A226C696D6974223B733A313A2235223B733A31313A22696D6167655F7769647468223B733A323A223830223B733A31323A22696D6167655F686569676874223B733A323A223830223B733A393A226C61796F75745F6964223B733A313A2232223B733A383A22706F736974696F6E223B733A31313A22636F6C756D6E5F6C656674223B733A363A22737461747573223B733A313A2231223B733A31303A22736F72745F6F72646572223B733A313A2233223B7D7D, '1');
INSERT INTO `si_setting` VALUES ('26390', '0', 'mostviewed', 'mostviewed_module', 0x613A313A7B693A303B613A373A7B733A353A226C696D6974223B733A313A2235223B733A31313A22696D6167655F7769647468223B733A333A22313230223B733A31323A22696D6167655F686569676874223B733A333A22313230223B733A393A226C61796F75745F6964223B733A313A2232223B733A383A22706F736974696F6E223B733A31313A22636F6C756D6E5F6C656674223B733A363A22737461747573223B733A313A2231223B733A31303A22736F72745F6F72646572223B733A313A2232223B7D7D, '1');
INSERT INTO `si_setting` VALUES ('26332', '0', 'hotsell', 'hotsell_module', 0x613A313A7B693A303B613A373A7B733A353A226C696D6974223B733A313A2235223B733A31313A22696D6167655F7769647468223B733A323A223830223B733A31323A22696D6167655F686569676874223B733A323A223830223B733A393A226C61796F75745F6964223B733A313A2233223B733A383A22706F736974696F6E223B733A31313A22636F6C756D6E5F6C656674223B733A363A22737461747573223B733A313A2231223B733A31303A22736F72745F6F72646572223B733A313A2234223B7D7D, '1');
INSERT INTO `si_setting` VALUES ('31123', '0', 'config', 'config_debug', 0x30, '0');
INSERT INTO `si_setting` VALUES ('26362', '0', 'bestseller', 'bestseller_module', 0x613A323A7B693A303B613A373A7B733A353A226C696D6974223B733A313A2235223B733A31313A22696D6167655F7769647468223B733A323A223830223B733A31323A22696D6167655F686569676874223B733A323A223830223B733A393A226C61796F75745F6964223B733A313A2232223B733A383A22706F736974696F6E223B733A31313A22636F6C756D6E5F6C656674223B733A363A22737461747573223B733A313A2231223B733A31303A22736F72745F6F72646572223B733A313A2233223B7D693A313B613A373A7B733A353A226C696D6974223B733A323A223130223B733A31313A22696D6167655F7769647468223B733A323A223830223B733A31323A22696D6167655F686569676874223B733A323A223830223B733A393A226C61796F75745F6964223B733A323A223134223B733A383A22706F736974696F6E223B733A31343A22636F6E74656E745F626F74746F6D223B733A363A22737461747573223B733A313A2231223B733A31303A22736F72745F6F72646572223B733A313A2232223B7D7D, '1');
INSERT INTO `si_setting` VALUES ('26393', '0', 'ims', 'ims', 0x613A313A7B693A303B613A343A7B733A343A2274797065223B733A323A227171223B733A373A226163636F756E74223B733A353A223130303030223B733A343A2274657874223B733A31323A22E4BB85E5819AE6B58BE8AF95223B733A31303A22736F72745F6F72646572223B733A313A2231223B7D7D, '1');
INSERT INTO `si_setting` VALUES ('26388', '0', 'cates', 'cates_module', 0x613A313A7B693A303B613A383A7B733A31313A22696D6167655F7769647468223B733A333A22323330223B733A31323A22696D6167655F686569676874223B733A333A22333030223B733A393A226C61796F75745F6964223B733A313A2231223B733A383A22706F736974696F6E223B733A31313A22636F6E74656E745F746F70223B733A363A22737461747573223B733A313A2231223B733A343A2263617465223B733A323A223634223B733A353A22636F756E74223B733A313A2238223B733A31303A22736F72745F6F72646572223B733A313A2232223B7D7D, '1');
INSERT INTO `si_setting` VALUES ('26662', '0', 'cod', 'cod_total', 0x30, '0');
INSERT INTO `si_setting` VALUES ('26666', '0', 'cod', 'cod_sort_order', 0x35, '0');
INSERT INTO `si_setting` VALUES ('26707', '0', 'voucher', 'voucher_status', 0x31, '0');
INSERT INTO `si_setting` VALUES ('26708', '0', 'voucher', 'voucher_sort_order', '', '0');
INSERT INTO `si_setting` VALUES ('30848', '0', 'config', 'config_url', 0x687474703A2F2F77656978696E2E6675777570752E636F6D, '0');
INSERT INTO `si_setting` VALUES ('28894', '0', 'config', 'config_sms_url', 0x687474703A2F2F6478687474702E633132332E636E2F74782F, '0');
INSERT INTO `si_setting` VALUES ('30837', '0', 'config', 'config_captcha', 0x30, '0');
INSERT INTO `si_setting` VALUES ('28895', '0', 'config', 'config_sms_ac', 0x353030383332373730303031, '0');
INSERT INTO `si_setting` VALUES ('28896', '0', 'config', 'config_sms_authkey', 0x313233, '0');
INSERT INTO `si_setting` VALUES ('28897', '0', 'config', 'config_sms_cgid', '', '0');
INSERT INTO `si_setting` VALUES ('28898', '0', 'config', 'config_sms_csid', '', '0');
INSERT INTO `si_setting` VALUES ('31217', '0', 'config', 'config_seat', 0x3130, '0');
INSERT INTO `si_setting` VALUES ('31223', '0', 'config', 'config_sms_notice_mobile', '', '0');
INSERT INTO `si_setting` VALUES ('31220', '0', 'config', 'config_print_notice', 0x30, '0');
INSERT INTO `si_setting` VALUES ('30835', '0', 'config', 'config_bind_status', 0x32, '0');
INSERT INTO `si_setting` VALUES ('30079', '0', 'config', 'config_logo', '', '0');
INSERT INTO `si_setting` VALUES ('30836', '0', 'config', 'config_phone_login', 0x31, '0');
INSERT INTO `si_setting` VALUES ('31205', '0', 'config', 'config_latlng', 0x34362E3733323539362C3132392E393137393233, '0');
INSERT INTO `si_setting` VALUES ('29975', '0', 'weight', 'weight_17_rate', 0x35, '0');
INSERT INTO `si_setting` VALUES ('29976', '0', 'weight', 'weight_17_time', 0x3130, '0');
INSERT INTO `si_setting` VALUES ('29977', '0', 'weight', 'weight_17_status', 0x31, '0');
INSERT INTO `si_setting` VALUES ('30833', '0', 'config', 'config_store_type', 0x31, '0');
INSERT INTO `si_setting` VALUES ('31239', '0', 'config', 'config_wechat_appid', '', '0');
INSERT INTO `si_setting` VALUES ('31240', '0', 'config', 'config_wechat_appsecret', '', '0');
INSERT INTO `si_setting` VALUES ('31241', '0', 'config', 'config_wechat_token', '', '0');
INSERT INTO `si_setting` VALUES ('31242', '0', 'config', 'config_wechat_reply', '', '0');
INSERT INTO `si_setting` VALUES ('31243', '0', 'config', 'config_wechat_attention', '', '0');
INSERT INTO `si_setting` VALUES ('30260', '0', 'config', 'config_wechat_status', 0x30, '0');
INSERT INTO `si_setting` VALUES ('31235', '0', 'config', 'config_home_search', 0x30, '0');
INSERT INTO `si_setting` VALUES ('31234', '0', 'config', 'config_home_banner', 0x30, '0');
INSERT INTO `si_setting` VALUES ('31238', '0', 'config', 'config_home_image', '', '0');
INSERT INTO `si_setting` VALUES ('31244', '0', 'config', 'config_mail_protocol', 0x30, '0');
INSERT INTO `si_setting` VALUES ('31246', '0', 'config', 'config_smtp_host', 0x736D74702E71712E636F6D, '0');
INSERT INTO `si_setting` VALUES ('31248', '0', 'config', 'config_smtp_password', 0x6D696D61, '0');
INSERT INTO `si_setting` VALUES ('31245', '0', 'config', 'config_mail_parameter', 0x3132334071712E636F6D, '0');
INSERT INTO `si_setting` VALUES ('31221', '0', 'config', 'config_weixin_notice', 0x30, '0');

-- ----------------------------
-- Table structure for si_stock_status
-- ----------------------------
DROP TABLE IF EXISTS `si_stock_status`;
CREATE TABLE `si_stock_status` (
  `stock_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`stock_status_id`,`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_stock_status
-- ----------------------------
INSERT INTO `si_stock_status` VALUES ('7', '1', '有货');
INSERT INTO `si_stock_status` VALUES ('8', '1', '预定');
INSERT INTO `si_stock_status` VALUES ('5', '1', '缺货');
INSERT INTO `si_stock_status` VALUES ('6', '1', '2 - 3 天到货');

-- ----------------------------
-- Table structure for si_store
-- ----------------------------
DROP TABLE IF EXISTS `si_store`;
CREATE TABLE `si_store` (
  `store_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_bin NOT NULL,
  `url` varchar(255) COLLATE utf8_bin NOT NULL,
  `ssl` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`store_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_store
-- ----------------------------

-- ----------------------------
-- Table structure for si_tax_class
-- ----------------------------
DROP TABLE IF EXISTS `si_tax_class`;
CREATE TABLE `si_tax_class` (
  `tax_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`tax_class_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_tax_class
-- ----------------------------
INSERT INTO `si_tax_class` VALUES ('9', '增值税', '增值税', '2009-01-06 23:21:53', '2011-09-24 19:54:54');

-- ----------------------------
-- Table structure for si_tax_rate
-- ----------------------------
DROP TABLE IF EXISTS `si_tax_rate`;
CREATE TABLE `si_tax_rate` (
  `tax_rate_id` int(11) NOT NULL AUTO_INCREMENT,
  `geo_zone_id` int(11) NOT NULL DEFAULT '0',
  `tax_class_id` int(11) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1',
  `rate` decimal(7,4) NOT NULL DEFAULT '0.0000',
  `description` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`tax_rate_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_tax_rate
-- ----------------------------

-- ----------------------------
-- Table structure for si_unit
-- ----------------------------
DROP TABLE IF EXISTS `si_unit`;
CREATE TABLE `si_unit` (
  `unit_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`unit_id`,`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_unit
-- ----------------------------
INSERT INTO `si_unit` VALUES ('17', '1', '个');
INSERT INTO `si_unit` VALUES ('18', '1', '例');
INSERT INTO `si_unit` VALUES ('19', '1', '套');
INSERT INTO `si_unit` VALUES ('20', '1', '斤');

-- ----------------------------
-- Table structure for si_url_alias
-- ----------------------------
DROP TABLE IF EXISTS `si_url_alias`;
CREATE TABLE `si_url_alias` (
  `url_alias_id` int(11) NOT NULL AUTO_INCREMENT,
  `query` varchar(255) COLLATE utf8_bin NOT NULL,
  `keyword` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`url_alias_id`)
) ENGINE=MyISAM AUTO_INCREMENT=56694 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_url_alias
-- ----------------------------
INSERT INTO `si_url_alias` VALUES ('56640', 'common/home', 'home');
INSERT INTO `si_url_alias` VALUES ('56641', 'account/account', 'my-account');
INSERT INTO `si_url_alias` VALUES ('56642', 'checkout/voucher', 'voucher');
INSERT INTO `si_url_alias` VALUES ('56643', 'information/contact', 'contact-us');
INSERT INTO `si_url_alias` VALUES ('56644', 'account/return/insert', 'return-service');
INSERT INTO `si_url_alias` VALUES ('56645', 'information/sitemap', 'sitemap');
INSERT INTO `si_url_alias` VALUES ('56646', 'product/manufacturer', 'brands');
INSERT INTO `si_url_alias` VALUES ('56647', 'affiliate/account', 'affiliate');
INSERT INTO `si_url_alias` VALUES ('56648', 'affiliate/register', 'affiliate-register');
INSERT INTO `si_url_alias` VALUES ('56649', 'affiliate/login', 'affiliate-login');
INSERT INTO `si_url_alias` VALUES ('56650', 'affiliate/edit', 'affiliate-edit');
INSERT INTO `si_url_alias` VALUES ('56651', 'affiliate/payment', 'affiliate-payment');
INSERT INTO `si_url_alias` VALUES ('56652', 'affiliate/password', 'affiliate-password');
INSERT INTO `si_url_alias` VALUES ('56653', 'affiliate/tracking', 'affiliate-tracking');
INSERT INTO `si_url_alias` VALUES ('56654', 'affiliate/transaction', 'affiliate-transaction');
INSERT INTO `si_url_alias` VALUES ('56655', 'affiliate/forgotten', 'affiliate-forgotten');
INSERT INTO `si_url_alias` VALUES ('56656', 'affiliate/logout', 'affiliate-logout');
INSERT INTO `si_url_alias` VALUES ('56657', 'product/special', 'special');
INSERT INTO `si_url_alias` VALUES ('56658', 'account/order', 'order-history');
INSERT INTO `si_url_alias` VALUES ('56659', 'account/order/info', 'order-detail');
INSERT INTO `si_url_alias` VALUES ('56660', 'account/wishlist', 'wishlist');
INSERT INTO `si_url_alias` VALUES ('56661', 'account/login', 'login');
INSERT INTO `si_url_alias` VALUES ('56662', 'account/logout', 'logout');
INSERT INTO `si_url_alias` VALUES ('56663', 'checkout/checkout', 'checkout');
INSERT INTO `si_url_alias` VALUES ('56664', 'product/compare', 'compare');
INSERT INTO `si_url_alias` VALUES ('56665', 'account/newsletter', 'newsletter');
INSERT INTO `si_url_alias` VALUES ('56666', 'account/forgotten', 'forgotten');
INSERT INTO `si_url_alias` VALUES ('56667', 'checkout/cart', 'cart');
INSERT INTO `si_url_alias` VALUES ('56668', 'account/register', 'register');
INSERT INTO `si_url_alias` VALUES ('56669', 'account/edit', 'edit-account');
INSERT INTO `si_url_alias` VALUES ('56670', 'account/address', 'address');
INSERT INTO `si_url_alias` VALUES ('56671', 'account/password', 'password');
INSERT INTO `si_url_alias` VALUES ('56672', 'account/download', 'mydownload');
INSERT INTO `si_url_alias` VALUES ('56673', 'account/reward', 'reward');
INSERT INTO `si_url_alias` VALUES ('56674', 'account/transaction', 'transaction');
INSERT INTO `si_url_alias` VALUES ('56675', 'account/return', 'return');
INSERT INTO `si_url_alias` VALUES ('56676', 'account/address/update', 'update-address');
INSERT INTO `si_url_alias` VALUES ('56677', 'account/address/delete', 'delete-address');
INSERT INTO `si_url_alias` VALUES ('56678', 'account/return/info', 'return-info');
INSERT INTO `si_url_alias` VALUES ('56679', 'account/invite', 'invite');
INSERT INTO `si_url_alias` VALUES ('56680', 'information_id=4', '关于我们');
INSERT INTO `si_url_alias` VALUES ('56681', 'information_id=5', '相关条款');
INSERT INTO `si_url_alias` VALUES ('56682', 'information_id=3', '购买说明');
INSERT INTO `si_url_alias` VALUES ('56683', 'information_id=6', '送货说明');
INSERT INTO `si_url_alias` VALUES ('56684', 'manufacturer_id=8', 'Apple');
INSERT INTO `si_url_alias` VALUES ('56685', 'manufacturer_id=9', 'Canon');
INSERT INTO `si_url_alias` VALUES ('56686', 'manufacturer_id=5', 'HTC');
INSERT INTO `si_url_alias` VALUES ('56687', 'manufacturer_id=7', 'Hewlett-Packard');
INSERT INTO `si_url_alias` VALUES ('56688', 'manufacturer_id=6', 'Palm');
INSERT INTO `si_url_alias` VALUES ('56689', 'manufacturer_id=10', 'Sony');

-- ----------------------------
-- Table structure for si_user
-- ----------------------------
DROP TABLE IF EXISTS `si_user`;
CREATE TABLE `si_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_group_id` int(11) NOT NULL,
  `username` varchar(20) COLLATE utf8_bin NOT NULL DEFAULT '',
  `password` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `firstname` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `lastname` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `email` varchar(96) COLLATE utf8_bin NOT NULL DEFAULT '',
  `code` varchar(32) COLLATE utf8_bin NOT NULL,
  `ip` varchar(15) COLLATE utf8_bin NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `openid` varchar(64) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_user
-- ----------------------------
INSERT INTO `si_user` VALUES ('1', '1', 'wei', 'd69d8949e163fd84c2a5da50138df308', '7777', '', '', '', '1.191.158.144', '1', '2014-07-30 17:16:04', '');

-- ----------------------------
-- Table structure for si_user_group
-- ----------------------------
DROP TABLE IF EXISTS `si_user_group`;
CREATE TABLE `si_user_group` (
  `user_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_bin NOT NULL,
  `permission` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`user_group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_user_group
-- ----------------------------
INSERT INTO `si_user_group` VALUES ('1', '管理员', 0x613A323A7B733A363A22616363657373223B613A3132373A7B693A303B733A31373A22636174616C6F672F617474726962757465223B693A313B733A32333A22636174616C6F672F6174747269627574655F67726F7570223B693A323B733A31363A22636174616C6F672F63617465676F7279223B693A333B733A31363A22636174616C6F672F646F776E6C6F6164223B693A343B733A31393A22636174616C6F672F696E666F726D6174696F6E223B693A353B733A32303A22636174616C6F672F6D616E756661637475726572223B693A363B733A31353A22636174616C6F672F6D657373616765223B693A373B733A31313A22636174616C6F672F6E6176223B693A383B733A31343A22636174616C6F672F6F7074696F6E223B693A393B733A31353A22636174616C6F672F70726F64756374223B693A31303B733A31343A22636174616C6F672F726576696577223B693A31313B733A31353A22636174616C6F672F736974656D6170223B693A31323B733A32333A22636F6D6D6F6E2F646F616A617866696C6575706C6F6164223B693A31333B733A31383A22636F6D6D6F6E2F66696C656D616E61676572223B693A31343B733A31393A22636F6D6D6F6E2F6C6F63616C69736174696F6E223B693A31353B733A31333A22636F6D6D6F6E2F75706C6F6164223B693A31363B733A31353A22636F6D6D6F6E2F75706C6F61646572223B693A31373B733A31383A22636F6D6D6F6E2F757079756E2E636C617373223B693A31383B733A31333A2264657369676E2F62616E6E6572223B693A31393B733A31333A2264657369676E2F6C61796F7574223B693A32303B733A31343A22657874656E73696F6E2F66656564223B693A32313B733A31363A22657874656E73696F6E2F6D6F64756C65223B693A32323B733A31373A22657874656E73696F6E2F7061796D656E74223B693A32333B733A31383A22657874656E73696F6E2F7368697070696E67223B693A32343B733A31343A22657874656E73696F6E2F746F6F6C223B693A32353B733A31353A22657874656E73696F6E2F746F74616C223B693A32363B733A31363A22666565642F676F6F676C655F62617365223B693A32373B733A31393A22666565642F676F6F676C655F736974656D6170223B693A32383B733A31343A226C61796F75742F64656661756C74223B693A32393B733A31363A226C61796F75742F706172616D65746572223B693A33303B733A31333A226C61796F75742F7265706F7274223B693A33313B733A31373A226C6F63616C69736174696F6E2F63697479223B693A33323B733A32303A226C6F63616C69736174696F6E2F636F756E747279223B693A33333B733A32313A226C6F63616C69736174696F6E2F63757272656E6379223B693A33343B733A32313A226C6F63616C69736174696F6E2F67656F5F7A6F6E65223B693A33353B733A32313A226C6F63616C69736174696F6E2F6C616E6775616765223B693A33363B733A32353A226C6F63616C69736174696F6E2F6C656E6774685F636C617373223B693A33373B733A32323A226C6F63616C69736174696F6E2F6C6F67697374696373223B693A33383B733A32353A226C6F63616C69736174696F6E2F6F726465725F737461747573223B693A33393B733A32363A226C6F63616C69736174696F6E2F72657475726E5F616374696F6E223B693A34303B733A32363A226C6F63616C69736174696F6E2F72657475726E5F726561736F6E223B693A34313B733A32363A226C6F63616C69736174696F6E2F72657475726E5F737461747573223B693A34323B733A32353A226C6F63616C69736174696F6E2F73746F636B5F737461747573223B693A34333B733A32323A226C6F63616C69736174696F6E2F7461785F636C617373223B693A34343B733A31373A226C6F63616C69736174696F6E2F756E6974223B693A34353B733A32353A226C6F63616C69736174696F6E2F7765696768745F636C617373223B693A34363B733A31373A226C6F63616C69736174696F6E2F7A6F6E65223B693A34373B733A31343A226D6F64756C652F6163636F756E74223B693A34383B733A31363A226D6F64756C652F616666696C69617465223B693A34393B733A31333A226D6F64756C652F62616E6E6572223B693A35303B733A31373A226D6F64756C652F6265737473656C6C6572223B693A35313B733A31353A226D6F64756C652F6361726F7573656C223B693A35323B733A31353A226D6F64756C652F63617465676F7279223B693A35333B733A31323A226D6F64756C652F6361746573223B693A35343B733A31343A226D6F64756C652F6465616C646179223B693A35353B733A31353A226D6F64756C652F6665617475726564223B693A35363B733A31343A226D6F64756C652F686F7473656C6C223B693A35373B733A31383A226D6F64756C652F696E666F726D6174696F6E223B693A35383B733A31333A226D6F64756C652F6C6174657374223B693A35393B733A31373A226D6F64756C652F6D6F7374766965776564223B693A36303B733A31353A226D6F64756C652F6F6E6C696E65696D223B693A36313B733A31363A226D6F64756C652F736C69646573686F77223B693A36323B733A31343A226D6F64756C652F7370656369616C223B693A36333B733A31323A226D6F64756C652F73746F7265223B693A36343B733A31333A226D6F64756C652F766965776564223B693A36353B733A31343A226D6F64756C652F77656C636F6D65223B693A36363B733A31343A227061796D656E742F616C69706179223B693A36373B733A32313A227061796D656E742F62616E6B5F7472616E73666572223B693A36383B733A31313A227061796D656E742F636F64223B693A36393B733A32313A227061796D656E742F667265655F636865636B6F7574223B693A37303B733A31393A227061796D656E742F70705F7374616E64617264223B693A37313B733A31343A227061796D656E742F74656E706179223B693A37323B733A32373A227265706F72742F616666696C696174655F636F6D6D697373696F6E223B693A37333B733A32323A227265706F72742F637573746F6D65725F637265646974223B693A37343B733A32313A227265706F72742F637573746F6D65725F6F72646572223B693A37353B733A32323A227265706F72742F637573746F6D65725F726577617264223B693A37363B733A32343A227265706F72742F70726F647563745F707572636861736564223B693A37373B733A32313A227265706F72742F70726F647563745F766965776564223B693A37383B733A31313A227265706F72742F73616C65223B693A37393B733A31383A227265706F72742F73616C655F636F75706F6E223B693A38303B733A31373A227265706F72742F73616C655F6F72646572223B693A38313B733A31383A227265706F72742F73616C655F72657475726E223B693A38323B733A32303A227265706F72742F73616C655F7368697070696E67223B693A38333B733A31353A227265706F72742F73616C655F746178223B693A38343B733A31343A2273616C652F616666696C69617465223B693A38353B733A31333A2273616C652F6175746F5F73656F223B693A38363B733A31323A2273616C652F636F6E74616374223B693A38373B733A31313A2273616C652F636F75706F6E223B693A38383B733A31333A2273616C652F637573746F6D6572223B693A38393B733A31393A2273616C652F637573746F6D65725F67726F7570223B693A39303B733A373A2273616C652F696D223B693A39313B733A31303A2273616C652F6F72646572223B693A39323B733A31313A2273616C652F72657475726E223B693A39333B733A31323A2273616C652F766F7563686572223B693A39343B733A31383A2273616C652F766F75636865725F7468656D65223B693A39353B733A31333A2273656F2F75726C5F616C696173223B693A39363B733A31343A2273657474696E672F637573746F6D223B693A39373B733A31323A2273657474696E672F6D61696C223B693A39383B733A31373A2273657474696E672F706172616D65746572223B693A39393B733A31343A2273657474696E672F736572766572223B693A3130303B733A31353A2273657474696E672F73657474696E67223B693A3130313B733A31313A2273657474696E672F736D73223B693A3130323B733A31333A2273657474696E672F73746F7265223B693A3130333B733A31353A2273657474696E672F75706772616465223B693A3130343B733A31343A2273657474696E672F776563686174223B693A3130353B733A31373A227368697070696E672F636974796C696E6B223B693A3130363B733A31333A227368697070696E672F666C6174223B693A3130373B733A31333A227368697070696E672F66726565223B693A3130383B733A31333A227368697070696E672F6974656D223B693A3130393B733A31353A227368697070696E672F7069636B7570223B693A3131303B733A31353A227368697070696E672F776569676874223B693A3131313B733A31313A22746F6F6C2F6261636B7570223B693A3131323B733A31343A22746F6F6C2F6572726F725F6C6F67223B693A3131333B733A31363A22746F6F6C7365742F6175746F5F73656F223B693A3131343B733A31353A22746F6F6C7365742F736974656D6170223B693A3131353B733A31323A22746F74616C2F636F75706F6E223B693A3131363B733A31323A22746F74616C2F637265646974223B693A3131373B733A31343A22746F74616C2F68616E646C696E67223B693A3131383B733A31393A22746F74616C2F6C6F775F6F726465725F666565223B693A3131393B733A31323A22746F74616C2F726577617264223B693A3132303B733A31343A22746F74616C2F7368697070696E67223B693A3132313B733A31353A22746F74616C2F7375625F746F74616C223B693A3132323B733A393A22746F74616C2F746178223B693A3132333B733A31313A22746F74616C2F746F74616C223B693A3132343B733A31333A22746F74616C2F766F7563686572223B693A3132353B733A393A22757365722F75736572223B693A3132363B733A32303A22757365722F757365725F7065726D697373696F6E223B7D733A363A226D6F64696679223B613A3132373A7B693A303B733A31373A22636174616C6F672F617474726962757465223B693A313B733A32333A22636174616C6F672F6174747269627574655F67726F7570223B693A323B733A31363A22636174616C6F672F63617465676F7279223B693A333B733A31363A22636174616C6F672F646F776E6C6F6164223B693A343B733A31393A22636174616C6F672F696E666F726D6174696F6E223B693A353B733A32303A22636174616C6F672F6D616E756661637475726572223B693A363B733A31353A22636174616C6F672F6D657373616765223B693A373B733A31313A22636174616C6F672F6E6176223B693A383B733A31343A22636174616C6F672F6F7074696F6E223B693A393B733A31353A22636174616C6F672F70726F64756374223B693A31303B733A31343A22636174616C6F672F726576696577223B693A31313B733A31353A22636174616C6F672F736974656D6170223B693A31323B733A32333A22636F6D6D6F6E2F646F616A617866696C6575706C6F6164223B693A31333B733A31383A22636F6D6D6F6E2F66696C656D616E61676572223B693A31343B733A31393A22636F6D6D6F6E2F6C6F63616C69736174696F6E223B693A31353B733A31333A22636F6D6D6F6E2F75706C6F6164223B693A31363B733A31353A22636F6D6D6F6E2F75706C6F61646572223B693A31373B733A31383A22636F6D6D6F6E2F757079756E2E636C617373223B693A31383B733A31333A2264657369676E2F62616E6E6572223B693A31393B733A31333A2264657369676E2F6C61796F7574223B693A32303B733A31343A22657874656E73696F6E2F66656564223B693A32313B733A31363A22657874656E73696F6E2F6D6F64756C65223B693A32323B733A31373A22657874656E73696F6E2F7061796D656E74223B693A32333B733A31383A22657874656E73696F6E2F7368697070696E67223B693A32343B733A31343A22657874656E73696F6E2F746F6F6C223B693A32353B733A31353A22657874656E73696F6E2F746F74616C223B693A32363B733A31363A22666565642F676F6F676C655F62617365223B693A32373B733A31393A22666565642F676F6F676C655F736974656D6170223B693A32383B733A31343A226C61796F75742F64656661756C74223B693A32393B733A31363A226C61796F75742F706172616D65746572223B693A33303B733A31333A226C61796F75742F7265706F7274223B693A33313B733A31373A226C6F63616C69736174696F6E2F63697479223B693A33323B733A32303A226C6F63616C69736174696F6E2F636F756E747279223B693A33333B733A32313A226C6F63616C69736174696F6E2F63757272656E6379223B693A33343B733A32313A226C6F63616C69736174696F6E2F67656F5F7A6F6E65223B693A33353B733A32313A226C6F63616C69736174696F6E2F6C616E6775616765223B693A33363B733A32353A226C6F63616C69736174696F6E2F6C656E6774685F636C617373223B693A33373B733A32323A226C6F63616C69736174696F6E2F6C6F67697374696373223B693A33383B733A32353A226C6F63616C69736174696F6E2F6F726465725F737461747573223B693A33393B733A32363A226C6F63616C69736174696F6E2F72657475726E5F616374696F6E223B693A34303B733A32363A226C6F63616C69736174696F6E2F72657475726E5F726561736F6E223B693A34313B733A32363A226C6F63616C69736174696F6E2F72657475726E5F737461747573223B693A34323B733A32353A226C6F63616C69736174696F6E2F73746F636B5F737461747573223B693A34333B733A32323A226C6F63616C69736174696F6E2F7461785F636C617373223B693A34343B733A31373A226C6F63616C69736174696F6E2F756E6974223B693A34353B733A32353A226C6F63616C69736174696F6E2F7765696768745F636C617373223B693A34363B733A31373A226C6F63616C69736174696F6E2F7A6F6E65223B693A34373B733A31343A226D6F64756C652F6163636F756E74223B693A34383B733A31363A226D6F64756C652F616666696C69617465223B693A34393B733A31333A226D6F64756C652F62616E6E6572223B693A35303B733A31373A226D6F64756C652F6265737473656C6C6572223B693A35313B733A31353A226D6F64756C652F6361726F7573656C223B693A35323B733A31353A226D6F64756C652F63617465676F7279223B693A35333B733A31323A226D6F64756C652F6361746573223B693A35343B733A31343A226D6F64756C652F6465616C646179223B693A35353B733A31353A226D6F64756C652F6665617475726564223B693A35363B733A31343A226D6F64756C652F686F7473656C6C223B693A35373B733A31383A226D6F64756C652F696E666F726D6174696F6E223B693A35383B733A31333A226D6F64756C652F6C6174657374223B693A35393B733A31373A226D6F64756C652F6D6F7374766965776564223B693A36303B733A31353A226D6F64756C652F6F6E6C696E65696D223B693A36313B733A31363A226D6F64756C652F736C69646573686F77223B693A36323B733A31343A226D6F64756C652F7370656369616C223B693A36333B733A31323A226D6F64756C652F73746F7265223B693A36343B733A31333A226D6F64756C652F766965776564223B693A36353B733A31343A226D6F64756C652F77656C636F6D65223B693A36363B733A31343A227061796D656E742F616C69706179223B693A36373B733A32313A227061796D656E742F62616E6B5F7472616E73666572223B693A36383B733A31313A227061796D656E742F636F64223B693A36393B733A32313A227061796D656E742F667265655F636865636B6F7574223B693A37303B733A31393A227061796D656E742F70705F7374616E64617264223B693A37313B733A31343A227061796D656E742F74656E706179223B693A37323B733A32373A227265706F72742F616666696C696174655F636F6D6D697373696F6E223B693A37333B733A32323A227265706F72742F637573746F6D65725F637265646974223B693A37343B733A32313A227265706F72742F637573746F6D65725F6F72646572223B693A37353B733A32323A227265706F72742F637573746F6D65725F726577617264223B693A37363B733A32343A227265706F72742F70726F647563745F707572636861736564223B693A37373B733A32313A227265706F72742F70726F647563745F766965776564223B693A37383B733A31313A227265706F72742F73616C65223B693A37393B733A31383A227265706F72742F73616C655F636F75706F6E223B693A38303B733A31373A227265706F72742F73616C655F6F72646572223B693A38313B733A31383A227265706F72742F73616C655F72657475726E223B693A38323B733A32303A227265706F72742F73616C655F7368697070696E67223B693A38333B733A31353A227265706F72742F73616C655F746178223B693A38343B733A31343A2273616C652F616666696C69617465223B693A38353B733A31333A2273616C652F6175746F5F73656F223B693A38363B733A31323A2273616C652F636F6E74616374223B693A38373B733A31313A2273616C652F636F75706F6E223B693A38383B733A31333A2273616C652F637573746F6D6572223B693A38393B733A31393A2273616C652F637573746F6D65725F67726F7570223B693A39303B733A373A2273616C652F696D223B693A39313B733A31303A2273616C652F6F72646572223B693A39323B733A31313A2273616C652F72657475726E223B693A39333B733A31323A2273616C652F766F7563686572223B693A39343B733A31383A2273616C652F766F75636865725F7468656D65223B693A39353B733A31333A2273656F2F75726C5F616C696173223B693A39363B733A31343A2273657474696E672F637573746F6D223B693A39373B733A31323A2273657474696E672F6D61696C223B693A39383B733A31373A2273657474696E672F706172616D65746572223B693A39393B733A31343A2273657474696E672F736572766572223B693A3130303B733A31353A2273657474696E672F73657474696E67223B693A3130313B733A31313A2273657474696E672F736D73223B693A3130323B733A31333A2273657474696E672F73746F7265223B693A3130333B733A31353A2273657474696E672F75706772616465223B693A3130343B733A31343A2273657474696E672F776563686174223B693A3130353B733A31373A227368697070696E672F636974796C696E6B223B693A3130363B733A31333A227368697070696E672F666C6174223B693A3130373B733A31333A227368697070696E672F66726565223B693A3130383B733A31333A227368697070696E672F6974656D223B693A3130393B733A31353A227368697070696E672F7069636B7570223B693A3131303B733A31353A227368697070696E672F776569676874223B693A3131313B733A31313A22746F6F6C2F6261636B7570223B693A3131323B733A31343A22746F6F6C2F6572726F725F6C6F67223B693A3131333B733A31363A22746F6F6C7365742F6175746F5F73656F223B693A3131343B733A31353A22746F6F6C7365742F736974656D6170223B693A3131353B733A31323A22746F74616C2F636F75706F6E223B693A3131363B733A31323A22746F74616C2F637265646974223B693A3131373B733A31343A22746F74616C2F68616E646C696E67223B693A3131383B733A31393A22746F74616C2F6C6F775F6F726465725F666565223B693A3131393B733A31323A22746F74616C2F726577617264223B693A3132303B733A31343A22746F74616C2F7368697070696E67223B693A3132313B733A31353A22746F74616C2F7375625F746F74616C223B693A3132323B733A393A22746F74616C2F746178223B693A3132333B733A31313A22746F74616C2F746F74616C223B693A3132343B733A31333A22746F74616C2F766F7563686572223B693A3132353B733A393A22757365722F75736572223B693A3132363B733A32303A22757365722F757365725F7065726D697373696F6E223B7D7D);
INSERT INTO `si_user_group` VALUES ('10', '后厨', 0x613A313A7B733A363A22616363657373223B613A313A7B693A303B733A31303A2273616C652F6F72646572223B7D7D);
INSERT INTO `si_user_group` VALUES ('11', '前厅服务员', 0x613A323A7B733A363A22616363657373223B613A313A7B693A303B733A31303A2273616C652F6F72646572223B7D733A363A226D6F64696679223B613A313A7B693A303B733A31303A2273616C652F6F72646572223B7D7D);

-- ----------------------------
-- Table structure for si_voucher
-- ----------------------------
DROP TABLE IF EXISTS `si_voucher`;
CREATE TABLE `si_voucher` (
  `voucher_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(128) COLLATE utf8_bin NOT NULL,
  `code` varchar(10) COLLATE utf8_bin NOT NULL,
  `from_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `from_email` varchar(96) COLLATE utf8_bin NOT NULL,
  `to_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `to_email` varchar(96) COLLATE utf8_bin NOT NULL,
  `message` text COLLATE utf8_bin NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`voucher_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_voucher
-- ----------------------------

-- ----------------------------
-- Table structure for si_voucher_history
-- ----------------------------
DROP TABLE IF EXISTS `si_voucher_history`;
CREATE TABLE `si_voucher_history` (
  `voucher_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `voucher_id` int(11) NOT NULL,
  `order_id` varchar(128) COLLATE utf8_bin NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`voucher_history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_voucher_history
-- ----------------------------

-- ----------------------------
-- Table structure for si_voucher_theme
-- ----------------------------
DROP TABLE IF EXISTS `si_voucher_theme`;
CREATE TABLE `si_voucher_theme` (
  `voucher_theme_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`voucher_theme_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_voucher_theme
-- ----------------------------
INSERT INTO `si_voucher_theme` VALUES ('8', 'data/canon_eos_5d_2.jpg');
INSERT INTO `si_voucher_theme` VALUES ('7', 'data/gift-voucher-birthday.jpg');
INSERT INTO `si_voucher_theme` VALUES ('6', 'data/apple_logo.jpg');

-- ----------------------------
-- Table structure for si_voucher_theme_description
-- ----------------------------
DROP TABLE IF EXISTS `si_voucher_theme_description`;
CREATE TABLE `si_voucher_theme_description` (
  `voucher_theme_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`voucher_theme_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_voucher_theme_description
-- ----------------------------
INSERT INTO `si_voucher_theme_description` VALUES ('6', '1', '圣诞节');
INSERT INTO `si_voucher_theme_description` VALUES ('7', '1', '生日');
INSERT INTO `si_voucher_theme_description` VALUES ('8', '1', '其他');

-- ----------------------------
-- Table structure for si_weight_class
-- ----------------------------
DROP TABLE IF EXISTS `si_weight_class`;
CREATE TABLE `si_weight_class` (
  `weight_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `value` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  PRIMARY KEY (`weight_class_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_weight_class
-- ----------------------------
INSERT INTO `si_weight_class` VALUES ('1', '1.00000000');
INSERT INTO `si_weight_class` VALUES ('2', '1000.00000000');
INSERT INTO `si_weight_class` VALUES ('5', '2.20460000');
INSERT INTO `si_weight_class` VALUES ('6', '35.27400000');

-- ----------------------------
-- Table structure for si_weight_class_description
-- ----------------------------
DROP TABLE IF EXISTS `si_weight_class_description`;
CREATE TABLE `si_weight_class_description` (
  `weight_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) COLLATE utf8_bin NOT NULL,
  `unit` varchar(4) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`weight_class_id`,`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_weight_class_description
-- ----------------------------
INSERT INTO `si_weight_class_description` VALUES ('1', '1', '千克', 'kg');
INSERT INTO `si_weight_class_description` VALUES ('2', '1', '克', 'g');
INSERT INTO `si_weight_class_description` VALUES ('5', '1', '磅', 'lb');
INSERT INTO `si_weight_class_description` VALUES ('6', '1', '盎司', 'oz');

-- ----------------------------
-- Table structure for si_zone
-- ----------------------------
DROP TABLE IF EXISTS `si_zone`;
CREATE TABLE `si_zone` (
  `zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `code` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `name` varchar(128) COLLATE utf8_bin NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`zone_id`)
) ENGINE=MyISAM AUTO_INCREMENT=720 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_zone
-- ----------------------------
INSERT INTO `si_zone` VALUES ('719', '44', '', '道外区', '1');
INSERT INTO `si_zone` VALUES ('718', '44', '', '南岗区', '1');

-- ----------------------------
-- Table structure for si_zone_to_geo_zone
-- ----------------------------
DROP TABLE IF EXISTS `si_zone_to_geo_zone`;
CREATE TABLE `si_zone_to_geo_zone` (
  `zone_to_geo_zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `city_id` int(11) DEFAULT '0',
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `geo_zone_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`zone_to_geo_zone_id`)
) ENGINE=MyISAM AUTO_INCREMENT=141 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of si_zone_to_geo_zone
-- ----------------------------
INSERT INTO `si_zone_to_geo_zone` VALUES ('138', '0', '428', '718', '17', '2014-04-06 03:03:41', '0000-00-00 00:00:00');
INSERT INTO `si_zone_to_geo_zone` VALUES ('137', '0', '430', '719', '17', '2014-04-06 03:03:41', '0000-00-00 00:00:00');
INSERT INTO `si_zone_to_geo_zone` VALUES ('140', '0', '427', '718', '18', '2014-04-06 03:04:16', '0000-00-00 00:00:00');
INSERT INTO `si_zone_to_geo_zone` VALUES ('139', '0', '429', '719', '18', '2014-04-06 03:04:16', '0000-00-00 00:00:00');
